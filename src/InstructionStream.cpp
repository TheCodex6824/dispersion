/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Translation/InstructionStream.hpp"

using namespace Dispersion::Translation;
using Dispersion::Translation::InstructionStream;

InstructionStream::InstructionStream():regs(), regLookup(), insns() {}

const VirtualReg& InstructionStream::getOrCreateReg(std::uint64_t reg, int flags)
{
    if (auto r = regLookup.find(reg); r != regLookup.end())
    {
        return regs[r->second];
    }

    const auto& ref = regs.emplace_back(reg, flags);
    regLookup.emplace(reg, regs.size() - 1);
    return ref;
}

const VirtualReg* InstructionStream::getReg(std::uint64_t reg) const
{
    if (auto r = regLookup.find(reg); r != regLookup.end())
    {
        return &regs[r->second];
    }

    return nullptr;
}

std::span<const VirtualReg> InstructionStream::getRegs() const
{
    return std::span<const VirtualReg>(regs.data(), regs.size());
}

void InstructionStream::pushInsn(Instruction insn)
{
    insns.push_back(insn);
}

std::span<const Instruction> InstructionStream::getInsns() const
{
    return std::span<const Instruction>(insns.data(), insns.size());
}
