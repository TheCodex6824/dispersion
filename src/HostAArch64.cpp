/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifdef __aarch64__
#include "Host/Arm/HostArm.hpp"

#include "ArchInfo/AArch64.hpp"
#include "Translation/Arm/AArch64NativeInstructionEmitter.hpp"

const bool Dispersion::Host::HostAArch64::registered = registerHost(std::make_unique<HostAArch64>());

std::optional<int> Dispersion::Host::HostAArch64::getSyscallNumber(Translation::Syscall syscall) const
{
    auto ptr = ArchInfo::AArch64::syscallToNum().find(syscall);
    if (ptr != ArchInfo::AArch64::syscallToNum().end())
    {
        return std::make_optional(ptr->second);
    }

    return std::nullopt;
}

std::unique_ptr<Dispersion::Translation::NativeInstructionEmitter> Dispersion::Host::HostAArch64::createInstructionEmitter() const
{
    return std::make_unique<Dispersion::Translation::AArch64NativeInstructionEmitter>();
}
#endif
