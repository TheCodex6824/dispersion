/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "HostRegistry.hpp"

#include "Host.hpp"
#include "System.hpp"

bool Dispersion::Host::registerHost(std::unique_ptr<Host> h)
{
    return entries().try_emplace(std::string(h->name()), std::move(h)).second;
}

std::optional<std::reference_wrapper<const Dispersion::Host::Host>> Dispersion::Host::thisHost()
{
    std::endian e = getSystemEndianness();
    Arch a = getSystemArch();
    BitWidth w = getSystemWidth();
    for (auto& pair : entries())
    {
        Host* h = pair.second.get();
        if (h->endian() == e && h->arch() == a && h->width() == w)
        {
            return std::optional<std::reference_wrapper<const Host>>(*pair.second);
        }
    }

    return std::nullopt;
}

std::optional<std::reference_wrapper<const Dispersion::Host::Host>> Dispersion::Host::hostFromName(std::string_view name)
{
    auto f = entries().find(std::string(name));
    if (f != entries().end())
    {
        return std::optional<std::reference_wrapper<const Host>>(*f->second);
    }

    return std::nullopt;
}
