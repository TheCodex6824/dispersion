/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Target/X86/TargetX86.hpp"

#include <stdexcept>

const bool Dispersion::Target::TargetX86::registered = registerTarget(std::make_unique<Dispersion::Target::TargetX86>());

void Dispersion::Target::TargetX86::applyRelocation(RelocationType type, std::span<std::byte> memory, const RelocationInfo& reloc) const
{
    throw std::logic_error("Not yet implemented");
}

std::unique_ptr<Dispersion::Translation::InstructionTranslator> Dispersion::Target::TargetX86::createInstructionTranslator() const
{
   throw std::logic_error("Not yet implemented");
}

Dispersion::Translation::Syscall Dispersion::Target::TargetX86::translateSyscall(int num) const
{
   throw std::logic_error("Not yet implemented");
}
