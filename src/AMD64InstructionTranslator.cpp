/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Translation/X86/AMD64InstructionTranslator.hpp"

#include "Translation/X86/AMD64TranslationImpl.hpp"

#include <iostream>

using Dispersion::Translation::AMD64InstructionTranslator;

AMD64InstructionTranslator::AMD64InstructionTranslator(): lookup()
{
    lookup.fill(&AMD64TranslationImpl::invalid);
    lookup[0x04] = &AMD64TranslationImpl::add_04;
    lookup[0x05] = &AMD64TranslationImpl::add_05;
    lookup[0x14] = &AMD64TranslationImpl::adc_14;
    lookup[0x15] = &AMD64TranslationImpl::adc_15;
    lookup[0x0f] = &AMD64TranslationImpl::table_0f;
    lookup[0xc7] = &AMD64TranslationImpl::mov_c7;
    lookup[0x8d] = &AMD64TranslationImpl::lea;
    //lookup[0xb8] = &AMD64TranslationImpl::mov_b8;
    //lookup[0xb9] = &AMD64TranslationImpl::mov_b9;
    //lookup[0xba] = &AMD64TranslationImpl::mov_ba;
    //lookup[0xbb] = &AMD64TranslationImpl::mov_bb;
    //lookup[0xbc] = &AMD64TranslationImpl::mov_bc;
    //lookup[0xbd] = &AMD64TranslationImpl::mov_bd;
    lookup[0xbe] = &AMD64TranslationImpl::mov_be;
    //lookup[0xbf] = &AMD64TranslationImpl::mov_bf;
}

bool AMD64InstructionTranslator::isLegacyPrefix(std::uint8_t prefix) const
{
    switch(static_cast<LegacyPrefix::LegacyPrefix>(prefix))
    {
    case LegacyPrefix::AddrSize:
    case LegacyPrefix::CsOrBranchNotTaken:
    case LegacyPrefix::DsOrBranchTaken:
    case LegacyPrefix::Es:
    case LegacyPrefix::Fs:
    case LegacyPrefix::Gs:
    case LegacyPrefix::Lock:
    case LegacyPrefix::OpSize:
    case LegacyPrefix::Rep:
    case LegacyPrefix::RepNe:
    case LegacyPrefix::Ss:
        return true;
    default:
        return false;
    }
}

std::pair<Dispersion::Translation::InstructionStream, std::uint64_t> AMD64InstructionTranslator::translateBlock(Dispersion::ReaderPtr& stream)
{
    InstructionStream ret;
    std::unordered_set<LegacyPrefix::LegacyPrefix> prefixes;
    std::optional<Rex::Rex> rex;
    while (true)
    {
        prefixes.clear();
        std::uint8_t insn;
        while (true)
        {
            insn = stream.read<std::uint8_t>();
            if ((insn & 0xF0) == Rex::RexPrefix)
            {
                rex.emplace(insn);
            }
            else if (isLegacyPrefix(insn))
            {
                prefixes.insert(static_cast<LegacyPrefix::LegacyPrefix>(insn));
            }
            else
            {
                break;
            }
        }

        if (lookup[insn](ret, rex, prefixes, stream))
        {
            break;
        }
    }

    return std::make_pair(ret, stream.diff());
}
