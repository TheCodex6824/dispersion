/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "TargetRegistry.hpp"

#include "TargetExecutable.hpp"
#include "Target.hpp"

bool Dispersion::Target::registerTarget(std::unique_ptr<Target> t)
{
    return entries().try_emplace(std::string(t->name()), std::move(t)).second;
}

std::optional<std::reference_wrapper<const Dispersion::Target::Target>> Dispersion::Target::targetForExecutable(const TargetExecutable& exe)
{
    for (auto& pair : entries())
    {
        if (pair.second->executableSupported(exe))
        {
            return std::optional<std::reference_wrapper<const Target>>(*pair.second);
        }
    }

    return std::nullopt;
}

std::optional<std::reference_wrapper<const Dispersion::Target::Target>> Dispersion::Target::targetFromName(std::string_view name)
{
    auto f = entries().find(std::string(name));
    if (f != entries().end())
    {
        return std::optional<std::reference_wrapper<const Target>>(*f->second);
    }

    return std::nullopt;
}
