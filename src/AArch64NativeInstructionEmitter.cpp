/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifdef __aarch64__
#include "Translation/Arm/AArch64NativeInstructionEmitter.hpp"

#include "ArchInfo/AArch64.hpp"
#include "ByteHelper.hpp"
#include "Exec/ExecutionContext.hpp"
#include "Target/TargetExecutable.hpp"
#include "Utils.hpp"

#include <bitset>
#include <cassert>
#include <limits>
#include <unistd.h>
#include <iostream>

using Dispersion::Translation::NativeBlock;
using Dispersion::Translation::AArch64NativeInstructionEmitter;

std::vector<std::pair<Dispersion::Translation::RegFlags::RegFlags, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId>> AArch64NativeInstructionEmitter::makeRegPool()
{
    using namespace Dispersion::ArchInfo::AArch64::PhysRegId;
    using namespace Dispersion::Translation::RegFlags;
    return
    {
        {SyscallArg1, R0},// | SyscallRet1, R0},
        {SyscallArg2, R1},// | SyscallRet2, R1},
        {SyscallArg3, R2},
        {SyscallArg4, R3},
        {SyscallArg5, R4},
        {SyscallArg6, R5},
        //{0, R6},
        //{0, R7},
        {SyscallNum, R8},
        {0, R9},
        {0, R10},
        {0, R11},
        {0, R12},
        {0, R13},
        //{0, R14},
        //{0, R15},
        {0, R16},
        {0, R17},
        {0, R18},
        {0, R19},
        {0, R20},
        {0, R21},
        {0, R22},
        {0, R23},
        {0, R24},
        //{0, R25},
        {0, R26},
        {0, R27},
        {0, R28}//,
        //{FramePointer, R29},
        //{LinkReg, R30},
       //{StackPointer, R31}
    };
}

AArch64NativeInstructionEmitter::AArch64NativeInstructionEmitter(): enableFlags(false) {}

NativeBlock AArch64NativeInstructionEmitter::emitBlock(const std::pair<InstructionStream, std::uint64_t>& insns)
{
    for (const Translation::VirtualReg& reg : insns.first.getRegs())
    {
        std::cout << "reg" << reg.reg << " flags: " << reg.flags << std::endl;
    }
    for (const Translation::Instruction& insn : insns.first.getInsns())
    {
        std::cout << insn.toString() << std::endl;
    }

    // TODO move all this reg stuff to some utility class instead of the emitter

    auto regPool = makeRegPool();
    std::vector<CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>> intervals;
    std::vector<CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>*> activeIntervals;
    for (std::size_t pos = 0; pos < insns.first.getInsns().size(); ++pos)
    {
        const Operand& dest = insns.first.getInsns()[pos].dest;
        if (dest.type == OperandType::Reg)
        {
            for (auto i = activeIntervals.begin(); i != activeIntervals.end(); /* intentionally missing */)
            {
                CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>* interval = *i;
                if (interval->getVirtReg() == dest.val)
                {
                    std::cout << "Interval ended for reg " << (*i)->getPhysReg() << std::endl;
                    (*i)->setEnd(pos);
                    regPool.emplace_back(insns.first.getReg(dest.val)->flags, (*i)->getPhysReg());
                    i = activeIntervals.erase(i);
                }
                else
                {
                    ++i;
                }
            }

            bool yes = false;
            RegFlags::RegFlags neededFlags = insns.first.getReg(dest.val)->flags;
            for (auto i = regPool.begin(); i != regPool.end(); ++i)
            {
                if ((i->first == 0 && neededFlags == 0) || (i->first & neededFlags))
                {
                    std::cout << "Allocated reg " << dest.val << "->" << i->second << std::endl;
                    std::cout << "Reg flags for allocated reg: " << i->first << std::endl;
                    activeIntervals.push_back(&intervals.emplace_back(dest.val, i->second, pos));
                    regPool.erase(i);
                    yes = true;
                    break;
                }
            }

            if (!yes)
            {
                throw std::runtime_error("No reg with required flags available: " + std::to_string(insns.first.getReg(dest.val)->flags));
            }
        }
    }

    for (auto& interval : intervals)
    {
        if (interval.getEnd() == 0)
        {
            interval.setEnd(insns.first.getInsns().size());
        }
    }

    std::vector<std::byte> code;
    // TODO add prelude to load regs
    for (std::size_t pos = 0; pos < insns.first.getInsns().size(); ++pos)
    {
        std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>> active;
        for (const auto& i : intervals)
        {
            if (i.isInInterval(pos))
            {
                active.emplace_back(i);
            }
        }

        const Translation::Instruction& insn = insns.first.getInsns()[pos];
        switch (insn.op)
        {
        case Opcode::Nop:
            nop(insn, code, active);
            break;
        case Opcode::Load:
            load(insn, code, active);
            break;
        case Opcode::Add:
            add(insn, code, active);
            break;
        case Opcode::Sys:
            sys(insn, code, active);
            break;
        default:
            invalid(insn, code, active);
            break;
        }
    }

    // TODO remove when executing other blocks
    emitRet(code);
    return NativeBlock{createExecutableMemory(std::span(code.begin(), code.size())), insns.second};
}

void AArch64NativeInstructionEmitter::invalid(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals)
{
    throw std::runtime_error("Invalid IR opcode: " + std::string(opcodeToString(insn.op)));
}

void AArch64NativeInstructionEmitter::nop(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals)
{
    pushValue<std::uint32_t>(out, 0xd503201f);
}

void emitLoadImm16(std::vector<std::byte>& out, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId reg, std::uint16_t val, int shift, bool keep)
{
    std::bitset<32> bits;
    Dispersion::setBits<0, 5>(bits, reg);
    Dispersion::setBits<5, 16>(bits, val);
    Dispersion::setBits<21, 2>(bits, shift);
    bits[23] = 1;
    bits[24] = 0;
    bits[25] = 1;
    bits[26] = 0;
    bits[27] = 0;
    bits[28] = 1;
    bits[29] = keep;
    bits[30] = 1;
    bits[31] = 1;
    Dispersion::pushValue<std::uint32_t>(out, bits.to_ulong());
}

void emitLoadImm32(std::vector<std::byte>& out, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId reg, std::uint32_t val, int shift)
{
    emitLoadImm16(out, reg, static_cast<std::uint16_t>(val & 0xFFFF), shift, false);
    if ((val & 0xFFFF0000))
    {
        emitLoadImm16(out, reg, static_cast<std::uint16_t>((val >> 16) & 0xFFFF), shift + 1, true);
    }
}

void emitLoadImm64(std::vector<std::byte>& out, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId reg, std::uint64_t val)
{
    emitLoadImm16(out, reg, static_cast<std::uint16_t>(val & 0xFFFF), 0, false);
    if ((val & 0xFFFF0000))
    {
        emitLoadImm16(out, reg, static_cast<std::uint16_t>((val >> 16) & 0xFFFF), 1, true);
    }
    if ((val & 0xFFFF00000000))
    {
        emitLoadImm16(out, reg, static_cast<std::uint16_t>((val >> 32) & 0xFFFF), 2, true);
    }
    if ((val & 0xFFFF000000000000))
    {
        emitLoadImm16(out, reg, static_cast<std::uint16_t>((val >> 48) & 0xFFFF), 3, true);
    }
}

void emitLoadReg(std::vector<std::byte>& out, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId arg, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId dest)
{
    std::bitset<32> bits;
    Dispersion::setBits<0, 5>(bits, dest);
    Dispersion::setBits<5, 5>(bits, arg);
    Dispersion::setBits<10, 6>(bits, 0);
    Dispersion::setBits<16, 5>(bits, arg);
    bits[21] = 0;
    bits[22] = 0;
    bits[23] = 0;
    bits[24] = 0;
    bits[25] = 1;
    bits[26] = 0;
    bits[27] = 1;
    bits[28] = 0;
    bits[29] = 1;
    bits[30] = 0;
    bits[31] = 1;
    Dispersion::pushValue<std::uint32_t>(out, bits.to_ulong());
}

void AArch64NativeInstructionEmitter::load(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals)
{
    // TODO implement loading from mem
    assert(insn.arg1.type != OperandType::Mem);
    if (insn.arg1.type == OperandType::Reg)
    {
        ArchInfo::AArch64::PhysRegId::PhysRegId regArg = ArchInfo::AArch64::PhysRegId::R0;
        ArchInfo::AArch64::PhysRegId::PhysRegId regDest = ArchInfo::AArch64::PhysRegId::R0;
        for (const auto& i : intervals)
        {
            if (i.get().getVirtReg() == insn.dest.val)
            {
                regDest = i.get().getPhysReg();
                break;
            }

            if (i.get().getVirtReg() == insn.arg1.val)
            {
                regArg = i.get().getPhysReg();
                break;
            }
        }

        assert(regArg != regDest);
        emitLoadReg(out, regArg, regDest);
    }
    else
    {
        bool good = false;
        ArchInfo::AArch64::PhysRegId::PhysRegId reg;
        for (const auto& i : intervals)
        {
            if (i.get().getVirtReg() == insn.dest.val)
            {
                reg = i.get().getPhysReg();
                good = true;
                break;
            }
        }

        assert(good);
        if (insn.arg1.size == OperandSize::Size16)
        {
            emitLoadImm16(out, reg, static_cast<std::uint16_t>(insn.arg1.val), 0, false);
        }
        else if (insn.arg1.size == OperandSize::Size32)
        {
            emitLoadImm32(out, reg, static_cast<std::uint32_t>(insn.arg1.val), 0);
        }
        else
        {
            emitLoadImm64(out, reg, insn.arg1.val);
        }
    }
}

void emitAddImmSimple(const Dispersion::Translation::Instruction& insn, std::vector<std::byte>& out, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId arg, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId dest, std::uint16_t add)
{
    std::bitset<32> bits;
    Dispersion::setBits<0, 5>(bits, dest);
    Dispersion::setBits<5, 5>(bits, arg);
    Dispersion::setBits<10, 8>(bits, add);
    Dispersion::setBits<18, 4>(bits, 0);
    bits[22] = 0;
    bits[23] = 0;
    bits[24] = 1;
    bits[25] = 0;
    bits[26] = 0;
    bits[27] = 0;
    bits[28] = 1;
    bits[29] = 0;
    bits[30] = 0;
    bits[31] = 1;
    Dispersion::pushValue<std::uint32_t>(out, bits.to_ulong());
}

void AArch64NativeInstructionEmitter::add(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals)
{
    assert(insn.arg1.type == OperandType::Reg && insn.arg2.type == OperandType::Const && insn.dest.type == OperandType::Reg);
    ArchInfo::AArch64::PhysRegId::PhysRegId arg, dest;
    for (const auto& i : intervals)
    {
        if (i.get().getVirtReg() == insn.dest.val)
        {
            dest = i.get().getPhysReg();
        }
        if (i.get().getVirtReg() == insn.arg1.val)
        {
            arg = i.get().getPhysReg();
        }
    }

    // TODO try shifted add first to not take forever
    std::uint64_t toAdd = insn.arg2.val;
    while (toAdd > 0)
    {
        std::uint16_t addThisStep = static_cast<std::uint16_t>(std::min<std::uint64_t>(toAdd, 255));
        emitAddImmSimple(insn, out, toAdd == insn.arg2.val ? arg : dest, dest, addThisStep);
        toAdd -= addThisStep;
    }
}

void sysImpl(std::uintptr_t arg1, std::uintptr_t arg2, std::uintptr_t arg3, std::uintptr_t arg4, std::uintptr_t arg5, std::uintptr_t arg6, std::int64_t num)
{
    std::uintptr_t save0, save1, save2, save3, save4, save5, save6, save7, save8;
    asm volatile(
        "str x0, %[save0];"
        "str x1, %[save1];"
        "str x2, %[save2];"
        "str x3, %[save3];"
        "str x4, %[save4];"
        "str x5, %[save5];"
        "str x6, %[save6];"
        "str x7, %[save7];"
        "str x8, %[save8];"
        : [save0] "=m" (save0), [save1] "=m" (save1), [save2] "=m" (save2), [save3] "=m" (save3), [save4] "=m" (save4), [save5] "=m" (save5), [save6] "=m" (save6), [save7] "=m" (save7), [save8] "=m" (save8)
        : // none
        : // none
    );
    const Dispersion::Exec::ExecutionContext* ctx = Dispersion::Exec::getCurrentContext();
    Dispersion::Translation::Syscall call = ctx->getTarget().translateSyscall(num);
    num = ctx->getHost().getSyscallNumber(call).value();
    asm volatile(
        "ldr x8, %[num];"
        "ldr x0, %[arg1];"
        "ldr x1, %[arg2];"
        "ldr x2, %[arg3];"
        "ldr x3, %[arg4];"
        "ldr x4, %[arg5];"
        "ldr x5, %[arg6];"
        "svc #0;"
        "ldr x0, %[save0];"
        "ldr x1, %[save1];"
        "ldr x2, %[save2];"
        "ldr x3, %[save3];"
        "ldr x4, %[save4];"
        "ldr x5, %[save5];"
        "ldr x6, %[save6];"
        "ldr x7, %[save7];"
        "ldr x8, %[save8];"
        : // none
        : [num] "m" (num), [arg1] "m" (arg1), [arg2] "m" (arg2), [arg3] "m" (arg3), [arg4] "m" (arg4), [arg5] "m" (arg5), [arg6] "m" (arg6), [save0] "m" (save0), [save1] "m" (save1), [save2] "m" (save2), [save3] "m" (save3), [save4] "m" (save4), [save5] "m" (save5), [save6] "m" (save6), [save7] "m" (save7), [save8] "m" (save8)
        : // we want regs to be clobbered here
    );
}

void AArch64NativeInstructionEmitter::sys(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals)
{
    // TODO spill if R6/R7 is used (R6/R7 are disabled for now)
    emitLoadReg(out, ArchInfo::AArch64::PhysRegId::R8, ArchInfo::AArch64::PhysRegId::R6);
    // LOAD func addr
    emitLoadImm64(out, ArchInfo::AArch64::PhysRegId::R7, reinterpret_cast<std::uintptr_t>(&sysImpl));
    // TODO replace with stack push
    emitLoadReg(out, ArchInfo::AArch64::PhysRegId::R30, ArchInfo::AArch64::PhysRegId::R25);
    // BLR to helper
    std::bitset<32> bits;
    setBits<0, 5>(bits, 0);
    setBits<5, 5>(bits, ArchInfo::AArch64::PhysRegId::R7);
    bits[10] = 0;
    bits[11] = 0;
    bits[12] = 0;
    bits[13] = 0;
    bits[14] = 0;
    bits[15] = 0;
    bits[16] = 1;
    bits[17] = 1;
    bits[18] = 1;
    bits[19] = 1;
    bits[20] = 1;
    bits[21] = 1;
    bits[22] = 0;
    bits[23] = 0;
    bits[24] = 0;
    bits[25] = 1;
    bits[26] = 1;
    bits[27] = 0;
    bits[28] = 1;
    bits[29] = 0;
    bits[30] = 1;
    bits[31] = 1;
    pushValue<std::uint32_t>(out, bits.to_ulong());
    emitLoadReg(out, ArchInfo::AArch64::PhysRegId::R25, ArchInfo::AArch64::PhysRegId::R30);
}

void AArch64NativeInstructionEmitter::emitRet(std::vector<std::byte> &out)
{
    pushValue(out, 0xd65f03c0);
}

#endif
