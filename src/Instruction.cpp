/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Translation/Instruction.hpp"

using Dispersion::Translation::Operand;
using Dispersion::Translation::Instruction;

std::string Operand::toString() const
{
    switch(type)
    {
    case OperandType::None: return "N/A";
    case OperandType::Const: return "const " + std::to_string(val) + "";
    case OperandType::Reg: return "reg" + std::to_string(val) + "";
    case OperandType::Mem: return "mem @ " + std::to_string(val) + "";
    default: return "INVALID";
    }
}

std::string Instruction::toString() const
{
    return dest.toString() + " = " + std::string(opcodeToString(op)) + "(" + arg1.toString() + ", " + arg2.toString() + ")";
}
