/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Translation/X86/AMD64TranslationImpl.hpp"

#include "Translation/VirtualFlag.hpp"

#include <cassert>
#include <iostream>

using namespace Dispersion;
using namespace ArchInfo::AMD64;
using namespace Translation;
using Translation::AMD64TranslationImpl;

std::uint64_t AMD64TranslationImpl::sib(std::uint8_t modrm, std::uint8_t scale, std::uint64_t index, std::uint64_t base, ReaderPtr& stream)
{
    switch (modrm & ModMod)
    {
        case 0:
        {
            if (base == PhysRegId::Bp || base == PhysRegId::R13)
            {
                if (index == PhysRegId::Sp)
                {
                    return stream.read<std::int32_t>();
                }
                else
                {
                    // todo reimplement
                    return 0;
                    //return index.val * scale + stream.read<std::int32_t>();
                }
            }
            else
            {
                if (index == PhysRegId::Sp)
                {
                    return 0;
                    //return base.val;
                }
                else
                {
                    return 0;
                    //return base.val + index.val * scale;
                }
            }
        }
        case 1:
        {
            if (index == PhysRegId::Sp)
            {
                return 0;
                //return base.val + stream.read<std::int8_t>();
            }
            else
            {
                return 0;
                //return base.val + index.val * scale + stream.read<std::int8_t>();
            }
        }
        case 2:
        {
            if (index == PhysRegId::Sp)
            {
                return 0;
                //return base.val + stream.read<std::int32_t>();
            }
            else
            {
                return 0;
                //return base.val + index.val * scale + stream.read<std::int32_t>();
            }
        }
    }

    throw std::runtime_error("Invalid SIB mode");
}

AMD64TranslationImpl::ModArgs AMD64TranslationImpl::modrm(std::uint8_t prefix, ReaderPtr& stream)
{
    std::uint8_t modrm = stream.read<std::uint8_t>();
    std::uint8_t mod = (modrm & ModMod) >> 6;
    std::uint8_t reg = ((modrm & ModReg) >> 3) | ((prefix & Rex::R) << 3);
    std::uint8_t rm = (modrm & ModRm) | ((prefix & Rex::B) << 3);
    AMD64TranslationImpl::ModArgs args{AMD64TranslationImpl::ArgType::Reg, reg, AMD64TranslationImpl::ArgType::Reg, rm};
    if (mod == 0)
    {
        if (rm == PhysRegId::Sp || rm == PhysRegId::R12)
        {
            std::uint8_t sibVal = stream.read<std::uint8_t>();
            std::uint8_t scale = std::pow(2, (sibVal & SibScale) >> 6);
            int index = (sibVal & SibIndex) >> 3;
            int base = sibVal & SibBase;
            args.type2 = AMD64TranslationImpl::ArgType::Mem;
            // todo emit extra instructions to cover this
            args.val2 = -1;//sib(modrm, scale, index, base, stream);
        }
        else if (rm == PhysRegId::Bp || rm == PhysRegId::R13)
        {
            args.type2 = AMD64TranslationImpl::ArgType::Mem;
            args.val2 = stream.read<std::int32_t>();
        }
    }
    else if (mod != 3)
    {
        throw std::runtime_error("unimplemented modrm mode");
    }

    return args;
}

bool AMD64TranslationImpl::invalid(InstructionStream& insns, std::optional<Rex::Rex>, const std::unordered_set<LegacyPrefix::LegacyPrefix>&, ReaderPtr& stream)
{
    throw std::runtime_error(std::string("Invalid opcode at ") + std::to_string(stream.pos()));
}

bool AMD64TranslationImpl::table_0f(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    auto next = stream.read<std::uint8_t>();
    if (next == 0x05)
    {
        return syscall(insns, rex, prefixes, stream);
    }
    else
    {
        return invalid(insns, rex, prefixes, stream);
    }
}

bool AMD64TranslationImpl::syscall(InstructionStream& insns, std::optional<Rex::Rex>, const std::unordered_set<LegacyPrefix::LegacyPrefix>&, ReaderPtr&)
{
    insns.pushInsn(Instruction{Opcode::Sys, Operand(), Operand(), Operand()});
    return true;
}

bool AMD64TranslationImpl::add_04(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    PhysRegId::PhysRegId destId = PhysRegId::Ax;
    insns.pushInsn({Opcode::Add, {OperandType::Reg, OperandSize::Size8, destId}, {OperandType::Reg, OperandSize::Size8, destId},
                    {OperandType::Const, OperandSize::Size8, stream.read<std::uint8_t>()}});
    return false;
}

bool AMD64TranslationImpl::add_05(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    PhysRegId::PhysRegId destId = PhysRegId::Ax;
    Operand addend;
    OperandSize size;
    if (prefixes.contains(LegacyPrefix::OpSize))
    {
        size = OperandSize::Size16;
        addend = {OperandType::Const, size, stream.read<std::uint16_t>()};
    }
    else if ((rex.value_or(0) & Rex::W) == Rex::W)
    {
        size = OperandSize::Size64;
        addend = {OperandType::Const, size, stream.read<std::uint32_t>()}; // TODO sign extend 32 bit value
    }
    else
    {
        size = OperandSize::Size32;
        addend = {OperandType::Const, size, stream.read<std::uint32_t>()};
    }

    insns.pushInsn({Opcode::Add, {OperandType::Reg, size, destId}, {OperandType::Reg, size, destId}, addend, {}});
    return false;
}

bool AMD64TranslationImpl::adc_14(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    PhysRegId::PhysRegId destId = PhysRegId::Ax;
    insns.pushInsn({Opcode::Add, {OperandType::Reg, OperandSize::Size8, destId}, {OperandType::Reg, OperandSize::Size8, destId},
                    {OperandType::Const, OperandSize::Size8, stream.read<std::uint8_t>()}, {}});
    return false;
}

bool AMD64TranslationImpl::adc_15(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    PhysRegId::PhysRegId destId = PhysRegId::Ax;
    Operand addend;
    OperandSize size;
    if (prefixes.contains(LegacyPrefix::OpSize))
    {
        size = OperandSize::Size16;
        addend = {OperandType::Const, size, stream.read<std::uint16_t>()};
    }
    else if ((rex.value_or(0) & Rex::W) == Rex::W)
    {
        size = OperandSize::Size64;
        addend = {OperandType::Const, size, stream.read<std::uint32_t>()}; // TODO sign extend 32 bit value
    }
    else
    {
        size = OperandSize::Size32;
        addend = {OperandType::Const, size, stream.read<std::uint32_t>()};
    }

    insns.pushInsn({Opcode::Add, {OperandType::Reg, size, destId}, {OperandType::Reg, size, destId}, addend, {}}); // TODO flags
    return false;
}

bool AMD64TranslationImpl::mov_88(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    return true;
}

bool AMD64TranslationImpl::mov_c7(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    auto args = modrm(rex.value(), stream);
    assert(args.type1 == ArgType::Reg);
    PhysRegId::PhysRegId destId = physRegFromIdentifier(args.val2);
    const VirtualReg& next = insns.getOrCreateReg(destId, physRegFlags(destId));
    insns.pushInsn({Opcode::Load, {OperandType::Const, OperandSize::Size32, stream.read<std::uint32_t>()}, Operand(), {OperandType::Reg, OperandSize::Size64, next.reg}});
    return false;
}

bool AMD64TranslationImpl::mov_be(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    PhysRegId::PhysRegId destId = PhysRegId::Si;
    const VirtualReg& next = insns.getOrCreateReg(destId, physRegFlags(destId));
    insns.pushInsn({Opcode::Load, {OperandType::Const, OperandSize::Size64, stream.read<std::uint64_t>()}, Operand(), {OperandType::Reg, OperandSize::Size64, next.reg}});
    return false;
}

bool AMD64TranslationImpl::lea(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    auto args = modrm(rex.value(), stream);
    PhysRegId::PhysRegId destId = physRegFromIdentifier(args.val1);
    const VirtualReg& destLoad = insns.getOrCreateReg(destId, physRegFlags(destId));
    insns.pushInsn({Opcode::Load, {OperandType::Const, OperandSize::Size32, args.val2}, Operand(), {OperandType::Reg, OperandSize::Size64, destLoad.reg}});

    destId = physRegFromIdentifier(args.val1);
    const VirtualReg& destAdd = insns.getOrCreateReg(destId, physRegFlags(destId));
    insns.pushInsn({Opcode::Add, {OperandType::Reg, OperandSize::Size64, destLoad.reg}, {OperandType::Const, OperandSize::Size64, stream.pos()}, {OperandType::Reg, OperandSize::Size64, destAdd.reg}, {}});
    return false;
}

bool AMD64TranslationImpl::and_24(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    PhysRegId::PhysRegId opId = PhysRegId::Ax;
    const VirtualReg& next = insns.getOrCreateReg(opId, physRegFlags(opId));
    insns.pushInsn({Opcode::And, {OperandType::Reg, OperandSize::Size8, next.reg}, {OperandType::Const, OperandSize::Size8, stream.read<std::uint8_t>()}, {OperandType::Reg, OperandSize::Size8, next.reg}});
    return false;
}

bool AMD64TranslationImpl::and_25(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)
{
    bool ext = rex.has_value() && (rex.value() & Rex::W);
    bool sizeOverride = prefixes.contains(LegacyPrefix::OpSize);

    return false;
}
