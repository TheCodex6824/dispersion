/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ElfParser.hpp"
#include "ParseException.hpp"
#include "Utils.hpp"

#include <elf.h>
#include <ios>
#include <iostream>
#include <algorithm>
#include <string>

namespace
{
    constexpr std::array<char, 4> ElfMagic {ELFMAG0, ELFMAG1, ELFMAG2, ELFMAG3};

    inline std::uint16_t read16(std::istream& in, bool le)
    {
        return le ? (in.get() | (in.get() << 8)) : ((in.get() << 8) | in.get());
    }

    inline std::uint32_t read32(std::istream& in, bool le)
    {
        return le ? (in.get() | (in.get() << 8) | (in.get() << 16) | (in.get() << 24)) : ((in.get() << 24) | (in.get() << 16) | (in.get() << 8) | in.get());
    }

    inline std::uint64_t read64(std::istream& in, bool le)
    {
        return le ? (in.get() | (in.get() << 8) | (in.get() << 16) | (in.get() << 24) | (static_cast<std::uint64_t>(in.get()) << 32) | (static_cast<std::uint64_t>(in.get()) << 40) | (static_cast<std::uint64_t>(in.get()) << 48) | (static_cast<std::uint64_t>(in.get()) << 56)) :
                    ((static_cast<std::uint64_t>(in.get()) << 56) | (static_cast<std::uint64_t>(in.get()) << 48) | (static_cast<std::uint64_t>(in.get()) << 40) | (static_cast<std::uint64_t>(in.get()) << 32) | (in.get() << 24) | (in.get() << 16) | (in.get() << 8) | in.get());
    }

    void parseSectionHeader(Dispersion::Elf::ElfSectionHeader& h, std::istream& input, std::endian endian, Dispersion::BitWidth width)
    {
        using namespace Dispersion;

        bool le = endian == std::endian::little;
        h.name = read32(input, le);
        std::uint32_t w = read32(input, le);
        switch (w)
        {
        case SHT_NULL:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Null;
            break;
        case SHT_PROGBITS:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Progbits;
            break;
        case SHT_SYMTAB:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Symtab;
            break;
        case SHT_STRTAB:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Strtab;
            break;
        case SHT_RELA:
            h.type = Dispersion::Elf::ElfSectionHeaderType::RelA;
            break;
        case SHT_HASH:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Hash;
            break;
        case SHT_DYNAMIC:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Dynamic;
            break;
        case SHT_NOTE:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Note;
            break;
        case SHT_NOBITS:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Nobits;
            break;
        case SHT_REL:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Rel;
            break;
        case SHT_SHLIB:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Shlib;
            break;
        case SHT_DYNSYM:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Dynsym;
            break;
        case SHT_INIT_ARRAY:
            h.type = Dispersion::Elf::ElfSectionHeaderType::InitArray;
            break;
        case SHT_FINI_ARRAY:
            h.type = Dispersion::Elf::ElfSectionHeaderType::FiniArray;
            break;
        case SHT_PREINIT_ARRAY:
            h.type = Dispersion::Elf::ElfSectionHeaderType::PreInitArray;
            break;
        case SHT_GROUP:
            h.type = Dispersion::Elf::ElfSectionHeaderType::Group;
            break;
        case SHT_SYMTAB_SHNDX:
            h.type = Dispersion::Elf::ElfSectionHeaderType::SymtabShndx;
            break;
        default:
            if (w >= SHT_LOOS && w <= SHT_HIOS)
            {
                h.type = Dispersion::Elf::ElfSectionHeaderType::Os;
            }
            else if (w >= SHT_LOPROC && w <= SHT_HIPROC)
            {
                h.type = Dispersion::Elf::ElfSectionHeaderType::Proc;
            }
            else
            {
                throw ParseException("ELF: invalid segment type " + std::to_string(w));
            }
        }

        bool w64 = width == BitWidth::Width64;
        h.attrs = w64 ? read64(input, le) : read32(input, le);
        h.vAddr = w64 ? read64(input, le) : read32(input, le);
        h.offset = w64 ? read64(input, le) : read32(input, le);
        h.link = read32(input, le);
        h.info = read32(input, le);
        h.align = w64 ? read64(input, le) : read32(input, le);
        h.entrySize = w64 ? read64(input, le) : read32(input, le);
    }
}

std::unique_ptr<Dispersion::Elf::ElfHeader> Dispersion::Elf::parseElfHeader(std::istream& input)
{
    std::unique_ptr<ElfHeader> header = std::make_unique<ElfHeader>();
    std::array<char, 4> magic;
    input.read(magic.data(), magic.size());
    if (!std::equal(magic.begin(), magic.end(), ElfMagic.begin()))
    {
        throw ParseException("ELF: Invalid magic number");
    }

    std::uint8_t b = input.get();
    if (b == ELFCLASS64)
    {
        header->width = BitWidth::Width64;
    }
    else if (b == ELFCLASS32)
    {
        header->width = BitWidth::Width32;
    }
    else
    {
        throw ParseException("ELF: Invalid bit width");
    }

    bool le = true;
    b = input.get();
    if (b == ELFDATA2LSB)
    {
        header->endian = std::endian::little;
    }
    else if (b == ELFDATA2MSB)
    {
        header->endian = std::endian::big;
        le = false;
    }
    else
    {
        throw ParseException("ELF: Invalid endianness");
    }

    header->elfVersion = input.get();
    if (header->elfVersion != EV_CURRENT)
    {
        std::cout << "ELF: header version not " << EV_CURRENT << ", there might be problems\n";
    }

    b = input.get();
    switch (b)
    {
    case ELFOSABI_SYSV: // "none" type also counts as this
        header->abi = ElfAbi::SystemV;
        break;
    case ELFOSABI_HPUX:
        header->abi = ElfAbi::HpUx;
        break;
    case ELFOSABI_NETBSD:
        header->abi = ElfAbi::NetBsd;
        break;
    case ELFOSABI_GNU:
        header->abi = ElfAbi::GNU;
        break;
    case ELFOSABI_SOLARIS:
        header->abi = ElfAbi::Solaris;
        break;
    case ELFOSABI_IRIX:
        header->abi = ElfAbi::Irix;
        break;
    case ELFOSABI_FREEBSD:
        header->abi = ElfAbi::FreeBsd;
        break;
    case ELFOSABI_TRU64:
        header->abi = ElfAbi::Tru64;
        break;
    case ELFOSABI_ARM:
        header->abi = ElfAbi::Arm;
        break;
    case ELFOSABI_ARM_AEABI:
        header->abi = ElfAbi::ArmExtended;
        break;
    case ELFOSABI_OPENBSD:
        header->abi = ElfAbi::OpenBsd;
        break;
    case ELFOSABI_STANDALONE:
        header->abi = ElfAbi::Standalone;
        break;
    case ELFOSABI_MODESTO:
        header->abi = ElfAbi::Modesto;
        break;
    default:
        throw ParseException("ELF: invalid ABI");
    }

    header->abiVersion = input.get();
    input.ignore(7);
    int elfType = read16(input, le);
    switch (elfType)
    {
    case ET_NONE:
        header->type = ElfType::None;
        break;
    case ET_REL:
        header->type = ElfType::Reloc;
        break;
    case ET_EXEC:
        header->type = ElfType::Exec;
        break;
    case ET_DYN:
        header->type = ElfType::Dynamic;
        break;
    case ET_CORE:
        header->type = ElfType::Core;
        break;
    default:
        header->type = (elfType & 0xFF00) == 0xFF00 ? ElfType::Proc : ElfType::Os;
        break;
    }

    // fill out many arches for completeness and possible future implementation
    // we bail later if it's one that's in this list but not supported
    switch (read16(input, le))
    {
    case EM_386:
        header->arch = Arch::X86;
        break;
    case EM_X86_64:
        header->arch = Arch::Amd64;
        break;
    case EM_PPC:
        header->arch = Arch::Ppc;
        break;
    case EM_PPC64:
        header->arch = Arch::Ppc64;
        break;
    case EM_ARM:
        header->arch = Arch::AArch32;
        break;
    case EM_AARCH64:
        header->arch = Arch::AArch64;
        break;
    case EM_MIPS:
        header->arch = Arch::Mips;
        break;
    case EM_RISCV:
        header->arch = Arch::RiscV;
        break;
    default:
        throw ParseException("ELF: arch isn't currently supported by dispersion");
    }

    if (read32(input, le) != EV_CURRENT)
    {
        std::cout << "ELF: secondary header version not " << EV_CURRENT << ", there might be problems\n";
    }
    
    bool w64 = header->width == BitWidth::Width64;
    header->entry = w64 ? read64(input, le) : read32(input, le);
    header->programHeader = w64 ? read64(input, le) : read32(input, le);
    header->sectionHeader = w64 ? read64(input, le) : read32(input, le);
    header->archData = read32(input, le);

    header->headerSize = read16(input, le);
    if ((header->width == BitWidth::Width32 && header->headerSize != 52) || (header->width == BitWidth::Width64 && header->headerSize != 64))
    {
        std::cout << "ELF: nonstandard header size (" << header->headerSize << "), there might be a problem\n";
    }

    header->programHeaderEntrySize = read16(input, le);
    if ((header->width == BitWidth::Width32 && header->programHeaderEntrySize != 32) || (header->width == BitWidth::Width64 && header->programHeaderEntrySize != 56))
    {
        std::cout << "ELF: nonstandard program header size (" << header->programHeaderEntrySize << "), there might be a problem\n";
    }

    int extendedInfo = 0;
    header->numProgramHeaderEntries = read16(input, le);
    if (header->numProgramHeaderEntries >= PN_XNUM)
    {
        extendedInfo |= 1;
    }

    header->sectionHeaderEntrySize = read16(input, le);
    if ((header->width == BitWidth::Width32 && header->sectionHeaderEntrySize != 40) || (header->width == BitWidth::Width64 && header->sectionHeaderEntrySize != 64))
    {
        std::cout << "ELF: nonstandard section header size (" << header->sectionHeaderEntrySize << "), there might be a problem\n";
    }

    header->numSectionHeaderEntries = read16(input, le);
    if (header->numSectionHeaderEntries >= SHN_LORESERVE)
    {
        extendedInfo |= 2;
    }

    header->sectionNameEntry = read16(input, le);
    if (header->sectionNameEntry >= SHN_LORESERVE)
    {
        extendedInfo |= 4;
    }

    if (extendedInfo != 0)
    {
        input.seekg(header->sectionHeader, std::ios_base::beg);
        ElfSectionHeader ext;
        parseSectionHeader(ext, input, header->endian, header->width);
        if ((extendedInfo & 1) == 1)
        {
            header->numProgramHeaderEntries = ext.info;
        }
        if ((extendedInfo & 2) == 2)
        {
            header->numSectionHeaderEntries = ext.size;
        }
        if ((extendedInfo & 4) == 4)
        {
            header->sectionNameEntry = ext.link;
        }
    }

    return header;
}

std::vector<Dispersion::Elf::ElfProgramHeader> Dispersion::Elf::parseProgramHeaders(const ElfHeader& header, std::istream& input)
{
    std::vector<ElfProgramHeader> headers(header.numProgramHeaderEntries);
    input.seekg(header.programHeader);
    bool le = header.endian == std::endian::little;
    for (auto& h : headers)
    {
        std::uint32_t w = read32(input, le);
        switch (w)
        {
        case PT_NULL:
            h.type = ElfSegmentType::Null;
            break;
        case PT_LOAD:
            h.type = ElfSegmentType::Load;
            break;
        case PT_DYNAMIC:
            h.type = ElfSegmentType::Dynamic;
            break;
        case PT_INTERP:
            h.type = ElfSegmentType::Interp;
            break;
        case PT_NOTE:
            h.type = ElfSegmentType::Note;
            break;
        case PT_SHLIB:
            h.type = ElfSegmentType::Shlib;
            break;
        case PT_PHDR:
            h.type = ElfSegmentType::Phdr;
            break;
        case PT_TLS:
            h.type = ElfSegmentType::Tls;
            break;
        case PT_GNU_STACK:
            h.type = ElfSegmentType::GnuStack;
            break;
        default:
            if (w >= PT_LOOS && w <= PT_HIOS)
            {
                h.type = ElfSegmentType::Os;
            }
            else if (w >= PT_LOPROC && w <= PT_HIPROC)
            {
                h.type = ElfSegmentType::Proc;
            }
            else
            {
                throw ParseException("ELF: invalid segment type " + std::to_string(w));
            }
        }

        bool w64 = header.width == BitWidth::Width64;
        if (w64)
        {
            h.flags = read32(input, le);
        }

        h.offset = w64 ? read64(input, le) : read32(input, le);
        h.vAddr = w64 ? read64(input, le) : read32(input, le);
        h.pAddr = w64 ? read64(input, le) : read32(input, le);
        h.fileSize = w64 ? read64(input, le) : read32(input, le);
        h.memSize = w64 ? read64(input, le) : read32(input, le);
        if (!w64)
        {
            h.flags = read32(input, le);
        }

        h.align = w64 ? read64(input, le) : read32(input, le);
    }

    return headers;
}

std::vector<Dispersion::Elf::ElfSectionHeader> Dispersion::Elf::parseSectionHeaders(const ElfHeader& header, std::istream& input)
{
    std::vector<ElfSectionHeader> headers(header.numSectionHeaderEntries);
    input.seekg(header.sectionHeader);
    for (auto& h : headers)
    {
        parseSectionHeader(h, input, header.endian, header.width);
    }

    return headers;
}

std::vector<Dispersion::Elf::ElfDynamicEntry> Dispersion::Elf::parseDynamicEntries(const ElfProgramHeader& h, BitWidth width, std::endian endian, const std::byte* data)
{
    bool w64 = width == BitWidth::Width64;
    std::size_t structLen = w64 ? 16 : 8;
    std::vector<ElfDynamicEntry> entries;
    entries.reserve(h.fileSize / structLen);
    for (std::size_t i = 0; i < h.fileSize; i += structLen) {
        std::uint64_t tag = w64 ? memcpy_cast<std::uint64_t>(data + h.vAddr + i) : memcpy_cast<std::uint32_t>(data + h.vAddr + i);
        if (tag == DT_NULL)
        {
            break;
        }

        ElfDynamicEntry& e = entries.emplace_back();
        e.tag = static_cast<ElfDynamicTag>(tag);
        e.value = w64 ? memcpy_cast<std::uint64_t>(data + h.vAddr + i + 8) : memcpy_cast<std::uint32_t>(data + h.vAddr + i + 4);
    }

    return entries;
}

Dispersion::Target::RelocationInfo Dispersion::Elf::parseRelEntry(BitWidth width, std::endian endian, const std::byte* data)
{
    Target::RelocationInfo ret{};
    if (width == BitWidth::Width64)
    {
        ret.addr = memcpy_cast<std::uint64_t>(data);
        ret.type = memcpy_cast<std::uint32_t>(data + sizeof(std::uint64_t));
        ret.symIndex = memcpy_cast<std::uint32_t>(data + sizeof(std::uint64_t) + sizeof(std::uint32_t));
    }
    else
    {
        ret.addr = memcpy_cast<std::uint32_t>(data);
        ret.type = memcpy_cast<std::uint16_t>(data + sizeof(std::uint32_t));
        ret.symIndex = memcpy_cast<std::uint16_t>(data + sizeof(std::uint32_t) + sizeof(std::uint16_t));
    }

    return ret;
}

Dispersion::Target::RelocationInfo Dispersion::Elf::parseRelaEntry(BitWidth width, std::endian endian, const std::byte* data)
{
    Target::RelocationInfo ret{};
    if (width == BitWidth::Width64)
    {
        ret.addr = memcpy_cast<std::uint64_t>(data);
        ret.type = memcpy_cast<std::uint32_t>(data + sizeof(std::uint64_t));
        ret.symIndex = memcpy_cast<std::uint32_t>(data + sizeof(std::uint64_t) + sizeof(std::uint32_t));
        ret.addend = std::make_optional(memcpy_cast<std::int64_t>(data + sizeof(std::uint64_t) + 2 * sizeof(std::uint32_t)));
    }
    else
    {
        ret.addr = memcpy_cast<std::uint32_t>(data);
        ret.type = memcpy_cast<std::uint16_t>(data + sizeof(std::uint32_t));
        ret.symIndex = memcpy_cast<std::uint16_t>(data + sizeof(std::uint32_t) + sizeof(std::uint16_t));
        ret.addend = std::make_optional(memcpy_cast<std::int32_t>(data + sizeof(std::uint32_t) + 2 * sizeof(std::uint16_t)));
    }

    return ret;
}
