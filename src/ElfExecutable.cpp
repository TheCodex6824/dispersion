/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "ElfExecutable.hpp"
#include "ExecutableException.hpp"
#include "ElfParser.hpp"
#include "Target.hpp"
#include "Utils.hpp"

#include <elf.h>
#include <sys/mman.h>
#include <stdexcept>
#include <string>
#include <cstring>
#include <iostream>
#include <bit>

Dispersion::Elf::ElfExecutable::ElfExecutable(std::istream& input): ar(Arch::Unknown), order(std::endian::native), w(BitWidth::Width32), mem()
{
    std::fill(mem.data(), mem.data() + mem.size(), std::byte{0});
    std::unique_ptr<ElfHeader> header = parseElfHeader(input);
    ar = header->arch;
    order = header->endian;
    w = header->width;
    entry = header->entry;

    std::vector<ElfProgramHeader> loadable;
    programHeaders = parseProgramHeaders(*header, input);
    for (const auto& h : programHeaders)
    {
        if (h.type == ElfSegmentType::Load)
        {
            if (h.fileSize > h.memSize)
            {
                throw ExecutableException("ELF: ill-formed binary, fileSize > memSize");
            }

            if (h.memSize != 0)
            {
                loadable.push_back(h);
            }
        }
    }
    std::uint64_t totalSize = loadable[loadable.size() - 1].vAddr + loadable[loadable.size() - 1].memSize;

    const ElfSectionHeader* dynsym;
    const ElfSectionHeader* strings;
    std::vector<ElfSectionHeader> reloc;
    std::vector<ElfSectionHeader> sectionHeaders = parseSectionHeaders(*header, input);
    for (const auto& h : sectionHeaders)
    {
        if (h.type == ElfSectionHeaderType::Rel)
        {
            reloc.push_back(h);
        }
        else if (h.type == ElfSectionHeaderType::Dynsym)
        {
            dynsym = &h;
            strings = &sectionHeaders.at(h.link);
        }
    }

    mem = MemoryImage(totalSize);
    for (const auto& h : loadable)
    {
        std::byte* dest = &mem[h.vAddr];
        input.seekg(h.offset);
        if (h.fileSize > 0)
        {
            input.read(reinterpret_cast<char*>(dest), h.fileSize);
        }
    }
}

void Dispersion::Elf::ElfExecutable::targetSetup(const Target::Target& target)
{
    std::uint64_t relEntrySize = 0, relaEntrySize = 0;
    std::uint64_t relTotalSize = 0, relaTotalSize = 0;
    std::vector<std::uint64_t> relAddrs, relaAddrs;
    for (const auto& h : programHeaders)
    {
        if (h.type == ElfSegmentType::Dynamic)
        {
            std::vector<ElfDynamicEntry> dynamic = parseDynamicEntries(h, w, order, mem.data());
            for (const auto& d : dynamic)
            {
                switch (d.tag)
                {
                case ElfDynamicTag::Rel:
                    relAddrs.push_back(d.value);
                    break;
                case ElfDynamicTag::RelSz:
                    relTotalSize = d.value;
                    break;
                case ElfDynamicTag::RelEnt:
                    relEntrySize = d.value;
                    break;
                case ElfDynamicTag::RelA:
                    relaAddrs.push_back(d.value);
                    break;
                case ElfDynamicTag::RelASz:
                    relaTotalSize = d.value;
                    break;
                case ElfDynamicTag::RelAEnt:
                    relaEntrySize = d.value;
                    break;
                default: break;
                }
            }

            break;
        }
    }

    std::vector<Target::RelocationInfo> reloc;
    if (relEntrySize > 0 && relTotalSize > 0)
    {
        for (const auto& addr : relAddrs)
        {
            reloc.push_back(parseRelEntry(w, order, mem.data() + addr));
        }
    }
    if (relaEntrySize > 0 && relaTotalSize > 0)
    {
        for (const auto& addr : relaAddrs)
        {
            reloc.push_back(parseRelaEntry(w, order, mem.data() + addr));
        }
    }

    for (const Target::RelocationInfo& r : reloc) {
        target.applyRelocation(Target::RelocationType::Elf, std::span<std::byte>(mem.data(), mem.size()), r);
    }

    for (const auto& h : programHeaders)
    {
        if (h.type == ElfSegmentType::Load && h.memSize != 0)
        {
            int perms = PROT_NONE;
            if (h.flags & PF_R)
            {
                perms |= PROT_READ;
            }
            if (h.flags & PF_W)
            {
                perms |= PROT_WRITE;
            }
            if (h.flags & PF_X)
            {
                perms |= PROT_EXEC;
            }

            if (perms && mprotect(reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(&mem[h.vAddr]) & (~0xFFF)), h.memSize, perms) == -1)
            {
                throw std::runtime_error("Failed to protect executable memory: " + std::string(std::strerror(errno)));
            }
        }
    }
}
