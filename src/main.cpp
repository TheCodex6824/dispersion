/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Elf/ElfExecutable.hpp"
#include "Exec/ExecutionContext.hpp"
#include "Host/HostRegistry.hpp"
#include "Target/TargetRegistry.hpp"

#include <iostream>
#include <fstream>

int main(int argc, char** argv)
{
    if (argc == 2)
    {
        auto h = Dispersion::Host::thisHost();
        if (!h.has_value())
        {
            std::cout << "Error: host configuration not found" << std::endl;
            return 1;
        }
        const Dispersion::Host::Host& host = h->get();

        std::ifstream input(argv[1]);
        input.exceptions(std::ios_base::badbit | std::ios_base::failbit | std::ios_base::eofbit);
        Dispersion::Elf::ElfExecutable exe(input);
        auto t = Dispersion::Target::targetForExecutable(exe);
        if (!t.has_value())
        {
            std::cout << "Error: no valid target found for executable" << std::endl;
            return 1;
        }

        const Dispersion::Target::Target& target = t->get();
        exe.targetSetup(target);
        Dispersion::Exec::ExecutionContext context(exe, target, host);
        context.setInstructionPointer(exe.entryAddr());
        context.execute();
    }
    else
    {
        std::cout << "Usage: " << argv[0] << " <path_to_exe>" << std::endl;
    }

    return 0;
}
