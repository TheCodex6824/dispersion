/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Target/X86/X86Impl.hpp"

#include <limits>

std::uint64_t Dispersion::Target::X86::carry(std::uint64_t op1, std::uint64_t op2)
{
    return (op1 > 0 && op2 > std::numeric_limits<std::uint64_t>::max() - op1) ||
            (op2 > 0 && op1 > std::numeric_limits<std::uint64_t>::max() - op2);
}
