/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Translation/BlockCache.hpp"

#include "Utils.hpp"

using Dispersion::Translation::NativeBlock;
using Dispersion::Translation::BlockCache;

BlockCache::BlockCache(InstructionTranslator& tr, NativeInstructionEmitter& e): translator(tr), emitter(e), blocks() {}

BlockCache::~BlockCache() noexcept
{
    for (auto& p : blocks)
    {
        NativeBlock& b = p.second;
        if (b.code.data() != nullptr)
        {
            freeExecutableMemory(b.code);
        }
    }
}

const NativeBlock& BlockCache::getOrTranslateBlock(std::uint64_t addr, ReaderPtr& ptr)
{
    if (auto b = blocks.find(addr); b != blocks.end())
    {
        return b->second;
    }

    return blocks.emplace(addr, emitter.emitBlock(translator.translateBlock(ptr))).first->second;
}
