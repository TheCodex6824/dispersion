/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "Exec/ExecutionContext.hpp"

#include "Utils.hpp"
#include "Target/Target.hpp"
#include "Target/TargetExecutable.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>
#include <unistd.h>

using Dispersion::Exec::ExecutionContext;

ExecutionContext::ExecutionContext(Target::TargetExecutable& e, const Target::Target& t, const Host::Host& h): exe(e), target(t), host(h), translator(t.createInstructionTranslator()),
    emitter(h.createInstructionEmitter()), cache(*translator, *emitter), pc() {}

void ExecutionContext::execute()
{
    ReaderPtr stream(exe.data());
    stream.seek(pc);
    detail::currentContext = this;
    while (true)
    {
        stream.save();
        const Translation::NativeBlock& b = cache.getOrTranslateBlock(pc, stream);
        pc += b.targetInsnLength;
        reinterpret_cast<void (*)()>(b.code.data())();
    }
}

std::uint64_t ExecutionContext::getInstructionPointer() const
{
    return pc;
}

void ExecutionContext::setInstructionPointer(std::uint64_t p)
{
    pc = p;
}

Dispersion::Target::TargetExecutable& ExecutionContext::getExecutable()
{
    return exe;
}

const Dispersion::Target::TargetExecutable& ExecutionContext::getExecutable() const
{
    return exe;
}

const Dispersion::Host::Host& ExecutionContext::getHost() const
{
    return host;
}

const Dispersion::Target::Target& ExecutionContext::getTarget() const
{
    return target;
}
