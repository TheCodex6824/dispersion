/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#include "MemoryImage.hpp"

#include <sys/mman.h>
#include <stdexcept>
#include <string>
#include <cstring>
#include <algorithm>

Dispersion::MemoryImage::MemoryImage(): mem(nullptr), len(0) {}

Dispersion::MemoryImage::MemoryImage(std::size_t size): mem(nullptr), len(size)
{
    mem = reinterpret_cast<std::byte*>(mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
    if (!mem)
    {
        throw std::runtime_error("Failed to allocate memory image: " + std::string(std::strerror(errno)));
    }
}

Dispersion::MemoryImage::~MemoryImage() noexcept
{
    if (mem)
    {
        munmap(mem, len);
    }
}

Dispersion::MemoryImage::MemoryImage(MemoryImage&& other) noexcept
{
    mem = other.mem;
    len = other.len;
    other.mem = nullptr;
    other.len = 0;
}

Dispersion::MemoryImage& Dispersion::MemoryImage::operator=(MemoryImage&& other) noexcept
{
    mem = other.mem;
    len = other.len;
    other.mem = nullptr;
    other.len = 0;
    return *this;
}
