/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MEMORYIMAGE_HPP
#define MEMORYIMAGE_HPP

#include <memory>
#include <cstdint>

namespace Dispersion
{
    class MemoryImage
    {
        std::byte* mem;
        std::size_t len;

    public:
        MemoryImage();
        explicit MemoryImage(std::size_t size);
        ~MemoryImage() noexcept;
        MemoryImage(const MemoryImage&) = delete;
        MemoryImage& operator=(const MemoryImage&) = delete;
        MemoryImage(MemoryImage&& other) noexcept;
        MemoryImage& operator=(MemoryImage&& other) noexcept;

        inline std::byte* data()
        {
            return mem;
        }

        inline const std::byte* data() const
        {
            return mem;
        }

        inline std::uint64_t size() const
        {
            return len;
        }

        inline std::byte& operator[](std::size_t i)
        {
            return mem[i];
        }

        inline const std::byte& operator[](std::size_t i) const
        {
            return mem[i];
        }
    };
}

#endif // MEMORYIMAGE_HPP
