/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXECUTIONCONTEXT_HPP
#define EXECUTIONCONTEXT_HPP

#include "Host/Host.hpp"
#include "Translation/BlockCache.hpp"
#include "Translation/InstructionTranslator.hpp"
#include "Translation/NativeInstructionEmitter.hpp"

#include <memory>

namespace Dispersion::Target
{
    class Target;
    class TargetExecutable;
}

namespace Dispersion::Exec
{
    class ExecutionContext
    {
        Target::TargetExecutable& exe;
        const Target::Target& target;
        const Host::Host& host;

        std::unique_ptr<Translation::InstructionTranslator> translator;
        std::unique_ptr<Translation::NativeInstructionEmitter> emitter;
        Translation::BlockCache cache;
        std::vector<Translation::VirtualReg> regs;
        std::vector<std::uint64_t> regValues;
        std::uint64_t pc;

    public:
        ExecutionContext(Target::TargetExecutable& e, const Target::Target& t, const Host::Host& h);

        void execute();
        std::uint64_t getInstructionPointer() const;
        void setInstructionPointer(std::uint64_t p);
        Target::TargetExecutable& getExecutable();
        const Target::TargetExecutable& getExecutable() const;
        const Host::Host& getHost() const;
        const Target::Target& getTarget() const;
    };

    // TODO clean all this up (ugly hack to give syscall handler context instance)

    namespace detail
    {
        __attribute__((__used__))
        inline ExecutionContext* currentContext;
    }

    inline const ExecutionContext* getCurrentContext()
    {
        return detail::currentContext;
    }
}

#endif // EXECUTIONCONTEXT_HPP
