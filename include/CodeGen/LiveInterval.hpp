/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIVEINTERVAL_HPP
#define LIVEINTERVAL_HPP

#include <cstdint>

namespace Dispersion::CodeGen
{
    template<typename PhysReg>
    class LiveInterval
    {
        std::uint64_t reg;
        PhysReg physReg;
        std::size_t start;
        std::size_t end;

    public:
        LiveInterval(std::uint64_t r, PhysReg p, std::size_t s): reg(r), physReg(p), start(s), end(0) {}

        inline std::uint64_t getVirtReg() const
        {
            return reg;
        }

        inline void setStart(std::size_t s)
        {
            start = s;
        }

        inline std::size_t getStart() const
        {
            return start;
        }

        inline void setEnd(std::size_t e)
        {
            end = e;
        }

        inline std::size_t getEnd() const
        {
            return end;
        }

        inline PhysReg getPhysReg() const
        {
            return physReg;
        }

        inline void setPhysReg(PhysReg r)
        {
            physReg = r;
        }

        inline bool isInInterval(std::size_t s) const
        {
            return s >= start && s < end;
        }
    };
}

#endif // LIVEINTERVAL_HPP
