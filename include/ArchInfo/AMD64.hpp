/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AMD64_HPP
#define AMD64_HPP

#include "Utils.hpp"
#include "Translation/Syscall.hpp"
#include "Translation/VirtualReg.hpp"

#include <cassert>
#include <unordered_map>

namespace Dispersion::ArchInfo::AMD64
{
    inline std::unordered_map<int, Translation::Syscall>& numToSyscall()
    {
        using Translation::Syscall;
        static std::unordered_map<int, Syscall> ret
        {
            #include "Data/Syscall_AMD64.inc"
        };

        return ret;
    }

    namespace
    {
        inline std::unordered_map<Translation::Syscall, int> makeReverseLookup()
        {
            std::unordered_map<int, Translation::Syscall> base = numToSyscall();
            std::unordered_map<Translation::Syscall, int> ret;
            for (const auto& [k, v] : base)
            {
                ret.emplace(v, k);
            }

            return ret;
        }
    }

    inline std::unordered_map<Translation::Syscall, int>& syscallToNum()
    {
        static std::unordered_map<Translation::Syscall, int> ret = makeReverseLookup();
        return ret;
    }

    static constexpr std::uint8_t ModMod = 0xc0;
    static constexpr std::uint8_t ModReg = 0x38;
    static constexpr std::uint8_t ModRm = 0x07;

    static constexpr std::uint8_t SibScale = 0xc0;
    static constexpr std::uint8_t SibIndex = 0x38;
    static constexpr std::uint8_t SibBase = 0x07;

    namespace PhysRegId
    {
        enum PhysRegId
        {
            Ax = 0,
            Cx,
            Dx,
            Bx,
            Sp,
            Bp,
            Si,
            Di,
            R8,
            R9,
            R10,
            R11,
            R12,
            R13,
            R14,
            R15
        };
    };

    inline PhysRegId::PhysRegId physRegFromIdentifier(unsigned int id)
    {
        assert(id < 16);
        return static_cast<PhysRegId::PhysRegId>(id);
    }

    inline int physRegFlags(PhysRegId::PhysRegId id)
    {
        using namespace Translation::RegFlags;
        static std::array<int, 16> lookup
        {
            SyscallNum | SyscallRet1,
            0,
            SyscallArg3 | SyscallRet2,
            0,
            StackPointer,
            FramePointer,
            SyscallArg2,
            SyscallArg1,
            SyscallArg5,
            SyscallArg6,
            SyscallArg4,
            0,
            0,
            0,
            0,
            0
        };
        return lookup[id];
    }
}

#endif // AMD64_HPP
