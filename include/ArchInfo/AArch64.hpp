/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AARCH64_HPP
#define AARCH64_HPP

#include "Utils.hpp"
#include "Translation/Syscall.hpp"

#include <unordered_map>

namespace Dispersion::ArchInfo::AArch64
{
    inline std::unordered_map<int, Translation::Syscall>& numToSyscall()
    {
        using Translation::Syscall;
        static std::unordered_map<int, Syscall> ret
        {
            #include "Data/Syscall_AArch64.inc"
        };

        return ret;
    }

    namespace
    {
        inline std::unordered_map<Translation::Syscall, int> makeReverseLookup()
        {
            std::unordered_map<int, Translation::Syscall> base = numToSyscall();
            std::unordered_map<Translation::Syscall, int> ret;
            for (const auto& [k, v] : base)
            {
                ret.emplace(v, k);
            }

            return ret;
        }
    }

    inline std::unordered_map<Translation::Syscall, int>& syscallToNum()
    {
        static std::unordered_map<Translation::Syscall, int> ret = makeReverseLookup();
        return ret;
    }

    namespace PhysRegId
    {
        enum PhysRegId
        {
            R0,
            R1,
            R2,
            R3,
            R4,
            R5,
            R6,
            R7,
            R8,
            R9,
            R10,
            R11,
            R12,
            R13,
            R14,
            R15,
            R16,
            R17,
            R18,
            R19,
            R20,
            R21,
            R22,
            R23,
            R24,
            R25,
            R26,
            R27,
            R28,
            R29,
            R30,
            R31
        };
    };

}

#endif // AARCH64_HPP
