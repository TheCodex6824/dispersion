/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ELFEXECUTABLE_HPP
#define ELFEXECUTABLE_HPP

#include "TargetExecutable.hpp"
#include "ElfStructures.hpp"
#include "MemoryImage.hpp"

#include <istream>
#include <vector>
#include <optional>

namespace Dispersion::Elf
{
    class ElfExecutable : public Dispersion::Target::TargetExecutable
    {
        Arch ar;
        std::endian order;
        BitWidth w;
        std::uint64_t entry;
        std::vector<ElfProgramHeader> programHeaders;
        MemoryImage mem;

    public:
        explicit ElfExecutable(std::istream& input);

        inline Arch arch() const override
        {
            return ar;
        }

        inline std::endian endian() const override
        {
            return order;
        }

        inline BitWidth width() const override
        {
            return w;
        }

        inline std::span<std::byte> data() override
        {
            return std::span<std::byte>(mem.data(), mem.size());
        }

        inline std::span<const std::byte> data() const override
        {
            return std::span<const std::byte>(mem.data(), mem.size());
        }

        inline std::uint64_t entryAddr() override
        {
            return entry;
        }

        void targetSetup(const Dispersion::Target::Target& target) override;

    };
}

#endif // ELFEXECUTABLE_HPP
