/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ELFPARSER_HPP
#define ELFPARSER_HPP

#include "Enums.hpp"
#include "ElfStructures.hpp"
#include "RelocationInfo.hpp"

#include <memory>
#include <istream>
#include <array>
#include <string_view>
#include <vector>

namespace Dispersion::Elf
{
    std::unique_ptr<ElfHeader> parseElfHeader(std::istream& input);
    std::vector<ElfProgramHeader> parseProgramHeaders(const ElfHeader& header, std::istream& input);
    std::vector<ElfSectionHeader> parseSectionHeaders(const ElfHeader& header, std::istream& input);
    std::vector<ElfDynamicEntry> parseDynamicEntries(const ElfProgramHeader& h, BitWidth width, std::endian endian, const std::byte* data);

    Dispersion::Target::RelocationInfo parseRelEntry(BitWidth width, std::endian endian, const std::byte* data);
    Dispersion::Target::RelocationInfo parseRelaEntry(BitWidth width, std::endian endian, const std::byte* data);
}

#endif // ELFPARSER_HPP
