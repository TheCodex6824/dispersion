/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ELFSTRUCTURES_HPP
#define ELFSTRUCTURES_HPP

#include "Enums.hpp"

#include <cstdint>
#include <bit>
#include <optional>

namespace Dispersion::Elf
{
    enum class ElfAbi
    {
        SystemV = 0,
        HpUx,
        NetBsd,
        GNU,
        Solaris,
        Arm,
        ArmExtended,
        Aix,
        Irix,
        FreeBsd,
        Tru64,
        Modesto,
        OpenBsd,
        Standalone
    };

    enum class ElfType
    {
        None = 0,
        Reloc,
        Exec,
        Dynamic,
        Core,
        Os,
        Proc
    };

    struct ElfHeader
    {
        std::uint64_t entry;
        std::uint64_t programHeader;
        std::uint64_t sectionHeader;
        std::uint32_t archData;
        BitWidth width;
        std::endian endian;
        ElfAbi abi;
        ElfType type;
        Arch arch;
        std::uint16_t headerSize;
        std::uint16_t programHeaderEntrySize;
        std::uint16_t numProgramHeaderEntries;
        std::uint16_t sectionHeaderEntrySize;
        std::uint16_t numSectionHeaderEntries;
        std::uint16_t sectionNameEntry;
        std::uint8_t elfVersion;
        std::uint8_t abiVersion;
    };

    enum class ElfSegmentType
    {
        Null = 0,
        Load,
        Dynamic,
        Interp,
        Note,
        Shlib,
        Phdr,
        Tls,
        GnuStack,
        Os,
        Proc
    };

    struct ElfProgramHeader
    {
        std::uint64_t offset;
        std::uint64_t vAddr;
        std::uint64_t pAddr;
        std::uint64_t fileSize;
        std::uint64_t memSize;
        std::uint64_t align;
        ElfSegmentType type;
        std::uint32_t flags;
    };

    enum class ElfSectionHeaderType
    {
        Null = 0,
        Progbits,
        Symtab,
        Strtab,
        RelA,
        Hash,
        Dynamic,
        Note,
        Nobits,
        Rel,
        Shlib,
        Dynsym,
        InitArray,
        FiniArray,
        PreInitArray,
        Group,
        SymtabShndx,
        Os,
        Proc
    };

    namespace ElfSectionAttrs
    {
        constexpr std::uint64_t Write = 0x1;
        constexpr std::uint64_t Alloc = 0x2;
        constexpr std::uint64_t Exec = 0x4;
        constexpr std::uint64_t Merge = 0x10;
        constexpr std::uint64_t String = 0x20;
        constexpr std::uint64_t LinkInfo = 0x40;
        constexpr std::uint64_t LinkOrder = 0x80;
        constexpr std::uint64_t OsNonconforming = 0x100;
        constexpr std::uint64_t Group = 0x200;
        constexpr std::uint64_t Tls = 0x400;
        constexpr std::uint64_t Os = 0xFF00000;
        constexpr std::uint64_t Proc = 0xF0000000;
        constexpr std::uint64_t Ordered = 0x4000000;
        constexpr std::uint64_t Exclude = 0x8000000;
    }

    struct ElfSectionHeader
    {
        std::uint64_t attrs;
        std::uint64_t vAddr;
        std::uint64_t offset;
        std::uint64_t size;
        std::uint64_t align;
        std::uint64_t entrySize;
        ElfSectionHeaderType type;
        std::uint32_t name;
        std::uint32_t link;
        std::uint32_t info;
    };

    enum class ElfDynamicTag : std::uint32_t
    {
        Null = 0,
        Needed,
        PltRelSz,
        PltGot,
        Hash,
        Strtab,
        Symtab,
        RelA,
        RelASz,
        RelAEnt,
        StrSz,
        SymEnt,
        Init,
        Fini,
        Soname,
        RPath,
        Symbolic,
        Rel,
        RelSz,
        RelEnt,
        PltRel,
        Debug,
        TextRel,
        JmpRel,
        BindNow,
        InitArray,
        FiniArray,
        InitArraySz,
        FiniArraySz,
        GnuHash = 0x6ffffef5,
        Flags1 = 0x6ffffffb,
        LoOs = 0x60000000,
        HiOs = 0x6ffff000,
        LoProc = 0x70000000,
        HiProc = 0x7ffff000
    };

    struct ElfDynamicEntry
    {
        ElfDynamicTag tag;
        std::uint64_t value;
    };
}

#endif // ELFSTRUCTURES
