/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BYTETHELPER_HPP
#define BYTETHELPER_HPP

#include "Utils.hpp"

#include <initializer_list>
#include <iterator>
#include <vector>

namespace Dispersion
{
    template<typename Input>
    void pushValue(std::vector<std::byte>& dest, Input val)
    {
        constexprFor<0, sizeof(Input), 1>(
            [&dest, &val]
            (std::size_t index)
            {
                dest.push_back(static_cast<std::byte>((val & (0xFF << (index * 8))) >> (index * 8)));
            }
        );
    }

    template<std::size_t DestStart, std::size_t Length, typename Bitset, typename Input>
    constexpr void setBits(Bitset& bitset, Input val)
    {
        constexprFor<0, Length, 1>(
            [&bitset, &val]
            (std::size_t index)
            {
                bitset[index + DestStart] = val & (1 << index);
            }
        );
    }
}

#endif // BYTEHELPER_HPP
