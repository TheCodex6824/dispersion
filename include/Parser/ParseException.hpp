/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARSEEXCEPTION_HPP
#define PARSEEXCEPTION_HPP

#include "ElfStructures.hpp"

#include <fstream>
#include <array>
#include <string_view>

namespace Dispersion
{
    class ParseException : public std::runtime_error
    {
    public:
        explicit ParseException(const std::string& message) noexcept;
    };
}

#endif // PARSEEXCEPTION_HPP
