/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef READERPTR_HPP
#define READERPTR_HPP

#include "Utils.hpp"

#include <memory>
#include <span>
#include <type_traits>

namespace Dispersion
{
    class ReaderPtr
    {
        std::byte* start;
        std::byte* internal;
        std::byte* mark;

    public:
        explicit ReaderPtr(std::span<std::byte> wrap) noexcept: start(wrap.data()), internal(wrap.data()), mark(nullptr)
        {
            save();
        }

        void save() noexcept
        {
            mark = internal;
        }

        void restore() noexcept
        {
            internal = mark;
        }

        std::ptrdiff_t diff() noexcept
        {
            return internal - mark;
        }

        std::uintptr_t pos() noexcept
        {
            return internal - start;
        }

        void seek(std::size_t pos) noexcept
        {
            internal = start + pos;
        }

        void skip(std::size_t num) noexcept
        {
            internal += num;
        }

        template<typename T, typename = std::enable_if<std::is_trivial_v<T>>>
        T read() noexcept
        {
            T val = *reinterpret_cast<T*>(internal);
            internal += sizeof(T);
            return val;
        }
    };
}

#endif // READERPTR_HPP
