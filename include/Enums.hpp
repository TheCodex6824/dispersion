/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ENUMS_HPP
#define ENUMS_HPP

namespace Dispersion
{
    enum class BitWidth
    {
        Width32,
        Width64
    };

    enum class Arch
    {
        Unknown,
        Special,
        X86,
        Mips,
        Ppc,
        Ppc64,
        AArch32,
        Amd64,
        AArch64,
        RiscV
    };
}

#endif // ENUMS_HPP
