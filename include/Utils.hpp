/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTILS_HPP
#define UTILS_HPP

#include <cstdint>
#include <cstring>
#include <memory>
#include <span>
#include <type_traits>

#include <sys/mman.h>

namespace Dispersion
{
    inline std::uint32_t bSwap32(std::uint32_t x) noexcept
    {
        #ifdef __GNUG__
            return __builtin_bswap32(x);
        #else
            return (x >> 24) | ((x << 8) & 0xFF0000) | ((x >> 8) & 0xFF00) | (x << 24);
        #endif
    }

    inline std::uint64_t bSwap64(std::uint64_t x) noexcept
    {
        #ifdef __GNUG__
            return __builtin_bswap64(x);
        #else
            return (x >> 56) | ((x << 40) & 0xFF000000000000) | ((x << 24) & 0xFF0000000000) | ((x << 8) & 0xFF00000000) |
                    ((x >> 8) & 0xFF000000) | ((x >> 24) & 0xFF0000) | ((x >> 40) & 0xFF00) | (x << 56);
        #endif
    }

    template<auto Start, auto End, auto Inc, typename Func>
    inline constexpr void constexprFor(Func&& func)
    {
        if constexpr(Start < End)
        {
            func(Start);
            constexprFor<Start + Inc, End, Inc>(func);
        }
    }

    template<typename To> requires std::is_integral_v<To>
    inline To memcpy_cast(const void* f) noexcept
    {
        To ret = 0;
        std::memcpy(&ret, f, std::min(sizeof(const void*), sizeof(To)));
        return ret;
    }

    template<typename K, typename V, template<typename, typename> typename MapType>
    inline std::remove_cvref_t<MapType<V, K>> reverseMap(const std::remove_cvref_t<MapType<K, V>>& input)
    {
        MapType<V, K> ret;
        for (const auto& [k, v] : input)
            ret.emplace_hint(ret.cend(), v, k);

        return ret;
    }

    template<typename T>
    inline std::span<std::byte> createExecutableMemory(const std::span<T> mem)
    {
        void* addr = mmap(nullptr, mem.size_bytes(), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        if (!addr)
        {
            throw std::runtime_error("Could not allocate memory");
        }

        std::memcpy(addr, mem.data(), mem.size_bytes());
        if (mprotect(addr, mem.size_bytes(), PROT_READ | PROT_EXEC))
        {
            int errnoOld = errno;
            munmap(addr, mem.size_bytes());
            throw std::runtime_error("Could not protect memory: " + std::string(strerror(errnoOld)));
        }

        return std::span<std::byte>(reinterpret_cast<std::byte*>(addr), mem.size_bytes());
    }

    inline void freeExecutableMemory(std::span<std::byte> mem)
    {
        munmap(mem.data(), mem.size_bytes());
    }
}

#endif //UTILS_HPP
