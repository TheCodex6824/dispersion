/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXECUTABLE_HPP
#define EXECUTABLE_HPP

#include "Enums.hpp"
#include "MemoryImage.hpp"
#include "Target/Target.hpp"

#include <bit>
#include <memory>
#include <span>

namespace Dispersion::Target
{
    class TargetExecutable
    {
    public:
        virtual ~TargetExecutable() = default;

        virtual Arch arch() const = 0;
        virtual std::endian endian() const = 0;
        virtual BitWidth width() const = 0;
        virtual std::span<std::byte> data() = 0;
        virtual std::span<const std::byte> data() const = 0;
        virtual std::uint64_t entryAddr() = 0;

        virtual void targetSetup(const Target& t) = 0;
    };
}

#endif // EXECUTABLE_HPP
