/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TARGETX86_HPP
#define TARGETX86_HPP

#include "Enums.hpp"
#include "Host/Host.hpp"
#include "Exec/ExecutionContext.hpp"
#include "TargetExecutable.hpp"
#include "Target.hpp"
#include "TargetRegistry.hpp"
#include "X86Impl.hpp"

#include <string>

namespace Dispersion::Target
{
    template<Arch a, BitWidth w>
    class TargetGenericX86 : public Target
    {
    public:
        inline Arch arch() const override
        {
            return a;
        }

        inline BitWidth width() const override
        {
            return w;
        }

        inline std::endian endian() const override
        {
            return std::endian::little;
        }

        inline bool executableSupported(const TargetExecutable& exe) const override
        {
            return exe.arch() == arch() && exe.endian() == endian() && exe.width() == width();
        }

        inline const Translation::VirtualFlagDefinition& flagDefinition(std::uint64_t flag) const override
        {
            static std::array<Translation::VirtualFlagDefinition, 1> flags{{Translation::VirtualFlagCommonDefs::Carry, 1, &X86::carry}};
            return flags.at(flag);
        }
    };

    class TargetX86 : public TargetGenericX86<Arch::X86, BitWidth::Width32>
    {
        static const bool registered;

    public:
        inline std::string_view name() const override
        {
            return "x86";
        }

        void applyRelocation(RelocationType type, std::span<std::byte> memory, const RelocationInfo& reloc) const override;

        std::unique_ptr<Translation::InstructionTranslator> createInstructionTranslator() const override;
        Translation::Syscall translateSyscall(int num) const override;
    };

    class TargetAMD64 : public TargetGenericX86<Arch::Amd64, BitWidth::Width64>
    {
        static const bool registered;

    public:
        inline std::string_view name() const override
        {
            return "amd64";
        }

        void applyRelocation(RelocationType type, std::span<std::byte> memory, const RelocationInfo& reloc) const override;

        std::unique_ptr<Translation::InstructionTranslator> createInstructionTranslator() const override;
        Translation::Syscall translateSyscall(int num) const override;
    };
}

#endif // TARGETX86_HPP
