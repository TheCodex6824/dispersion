/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RELOCATIONINFO_HPP
#define RELOCATIONINFO_HPP

#include <cstdint>
#include <optional>

namespace Dispersion::Target
{
    enum class RelocationType
    {
        Elf = 0
    };

    struct RelocationInfo
    {
        std::uint64_t addr;
        std::uint32_t type;
        std::uint32_t symIndex;
        std::optional<std::int64_t> addend;
    };
}

#endif // RELOCATIONINFO_HPP
