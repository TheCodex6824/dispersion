/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TARGET_HPP
#define TARGET_HPP

#include "Enums.hpp"
#include "Host/Host.hpp"
#include "Target/RelocationInfo.hpp"
#include "Translation/VirtualFlag.hpp"

#include <bit>
#include <memory>
#include <span>
#include <string_view>

namespace Dispersion
{
    class MemoryImage;
}

namespace Dispersion::Translation
{
    class InstructionTranslator;
}

namespace Dispersion::Target
{
    class TargetExecutable;

    class Target
    {
    public:
        virtual ~Target() = default;

        virtual Arch arch() const = 0;
        virtual BitWidth width() const = 0;
        virtual std::endian endian() const = 0;
        virtual std::string_view name() const = 0;

        virtual bool executableSupported(const TargetExecutable& exe) const = 0;
        virtual void applyRelocation(RelocationType type, std::span<std::byte> memory, const RelocationInfo& reloc) const = 0;
        virtual std::unique_ptr<Translation::InstructionTranslator> createInstructionTranslator() const = 0;
        virtual Translation::Syscall translateSyscall(int num) const = 0;
        virtual const Translation::VirtualFlagDefinition& flagDefinition(std::uint64_t flag) const = 0;
    };
}

#endif // TARGET
