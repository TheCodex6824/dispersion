/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TARGETREGISTRY_HPP
#define TARGETREGISTRY_HPP

#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <optional>
#include <unordered_map>

namespace Dispersion::Target
{
    class Target;
    class TargetExecutable;

    namespace
    {
        inline std::unordered_map<std::string, std::unique_ptr<Target>>& entries()
        {
            static auto entries = std::unordered_map<std::string, std::unique_ptr<Target>>();
            return entries;
        }
    }

    bool registerTarget(std::unique_ptr<Target> t);
    std::optional<std::reference_wrapper<const Dispersion::Target::Target>> targetForExecutable(const TargetExecutable& exe);
    std::optional<std::reference_wrapper<const Dispersion::Target::Target>> targetFromName(std::string_view name);
}

#endif // TARGETREGISTRY
