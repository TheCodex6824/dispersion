/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HOSTREGISTRY_HPP
#define HOSTREGISTRY_HPP

#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <optional>
#include <unordered_map>

namespace Dispersion::Host
{
    class Host;

    namespace
    {
        inline std::unordered_map<std::string, std::unique_ptr<Host>>& entries()
        {
            static auto entries = std::unordered_map<std::string, std::unique_ptr<Host>>();
            return entries;
        }
    }

    bool registerHost(std::unique_ptr<Host> h);
    std::optional<std::reference_wrapper<const Dispersion::Host::Host>> thisHost();
    std::optional<std::reference_wrapper<const Dispersion::Host::Host>> hostFromName(std::string_view name);
}

#endif // TARGETREGISTRY
