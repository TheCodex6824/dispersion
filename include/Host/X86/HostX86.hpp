/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HOSTX86_HPP
#define HOSTX86_HPP
#if defined(__amd64__) || defined(_M_X64) || defined(__x86__) || defined(_M_X86)

#include "Enums.hpp"
#include "Host/Host.hpp"
#include "Host/HostRegistry.hpp"
#include "Utils.hpp"

#include <string>

namespace Dispersion::Host
{
    template<Arch a, BitWidth w>
    class HostGenericX86 : public Host
    {
    public:
        inline Arch arch() const override
        {
            return a;
        }

        inline BitWidth width() const override
        {
            return w;
        }

        inline std::endian endian() const override
        {
            return std::endian::little;
        }

        inline std::unique_ptr<Translation::NativeInstructionEmitter> createInstructionEmitter() const override
        {
            return std::unique_ptr<Translation::NativeInstructionEmitter>(nullptr);//throw std::runtime_error("Not implemented");
        }
    };

#if defined(__x86__) || defined(_M_X86)
    class HostX86 : public HostGenericX86<Arch::X86, BitWidth::Width32>
    {
        static const bool registered;

    public:
        inline std::string_view name() const override
        {
            return "x86";
        }

        std::optional<int> getSyscallNumber(Translation::Syscall) const override
        {
            throw std::runtime_error("Not implemented");
        }
    };
#elif defined(__amd64__) || defined(_M_X64)
    class HostAMD64 : public HostGenericX86<Arch::Amd64, BitWidth::Width64>
    {
        static const bool registered;

    public:
        inline std::string_view name() const override
        {
            return "amd64";
        }

        std::optional<int> getSyscallNumber(Translation::Syscall syscall) const override;
    };
#endif
}

#endif
#endif // HOSTX86
