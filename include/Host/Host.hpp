/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HOST_HPP
#define HOST_HPP

#include "Enums.hpp"
#include "Translation/NativeInstructionEmitter.hpp"
#include "Translation/Syscall.hpp"

#include <bit>
#include <memory>
#include <optional>
#include <string_view>

namespace Dispersion::Host
{
    class Host
    {
    public:
        virtual ~Host() = default;

        virtual Arch arch() const = 0;
        virtual BitWidth width() const = 0;
        virtual std::endian endian() const = 0;
        virtual std::string_view name() const = 0;

        virtual std::unique_ptr<Translation::NativeInstructionEmitter> createInstructionEmitter() const = 0;
        virtual std::optional<int> getSyscallNumber(Translation::Syscall) const = 0;
    };
}

#endif // HOST_HPP
