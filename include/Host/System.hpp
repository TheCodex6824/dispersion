/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include "Enums.hpp"

#include <bit>

namespace Dispersion::Host
{
    inline std::endian getSystemEndianness()
    {
        return std::endian::native;
    }

    inline Arch getSystemArch()
    {
#if defined(__amd64__) || defined(_M_X64)
        return Arch::Amd64;
#elif defined(__x86__) || defined(_M_X86)
        return Arch::X86;
#elif defined(__aarch64__)
        return Arch::AArch64;
#elif defined(__arm__)
        return Arch::AArch32;
#elif defined(__ppc64__)
        return Arch::Ppc64;
#elif defined(__ppc__)
        return Arch::Ppc;
#else
        return Arch::Unknown;
#endif
    }

    inline BitWidth getSystemWidth()
    {
#if __INTPTR_MAX__ == __INT64_MAX__
        return BitWidth::Width64;
#else
        return BitWidth::Width32;
#endif
    }
}

#endif // SYSTEM_HPP
