/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HOSTARM_HPP
#define HOSTARM_HPP
#if defined(__aarch64__) || defined(__arm__)

#include "Enums.hpp"
#include "Host/Host.hpp"
#include "Host/HostRegistry.hpp"
#include "Utils.hpp"

#include <string>

namespace Dispersion::Host
{
    template<Arch a, BitWidth w>
    class HostGenericArm : public Host
    {
    public:
        inline Arch arch() const override
        {
            return a;
        }

        inline BitWidth width() const override
        {
            return w;
        }

        inline std::endian endian() const override
        {
            return std::endian::native;
        }
    };

#ifdef __arm__
    class HostAArch32 : public HostGenericArm<Arch::AArch32, BitWidth::Width32>
    {
        static const bool registered;

    public:
        inline std::string_view name() const override
        {
            return "aarch32";
        }

        std::optional<int> getSyscallNumber(Translation::Syscall) const override
        {
            throw std::runtime_error("Not implemented");
        }

        inline std::unique_ptr<Translation::NativeInstructionEmitter> createInstructionEmitter() const override
        {
            throw std::runtime_error("Not implemented");
        }
    };
#elif defined(__aarch64__)
    class HostAArch64 : public HostGenericArm<Arch::AArch64, BitWidth::Width64>
    {
        static const bool registered;

    public:
        inline std::string_view name() const override
        {
            return "aarch64";
        }

        std::optional<int> getSyscallNumber(Translation::Syscall syscall) const override;

        std::unique_ptr<Translation::NativeInstructionEmitter> createInstructionEmitter() const override;
    };
#endif
}

#endif
#endif // HOSTARM_HPP
