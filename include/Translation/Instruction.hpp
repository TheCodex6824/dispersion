/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INSTRUCTION_HPP
#define INSTRUCTION_HPP

#include "Translation/Opcode.hpp"

#include <string>
#include <vector>

namespace Dispersion::Translation
{
    enum class OperandType : std::uint8_t
    {
        None,
        Const,
        Reg,
        Mem
    };

    enum class OperandSize : std::uint8_t
    {
        None,
        Size8,
        Size16,
        Size32,
        Size64
    };

    struct Operand
    {
        OperandType type;
        OperandSize size;
        std::uint64_t val;

        std::string toString() const;
    };

    struct Instruction
    {
        Opcode op;
        Operand arg1;
        Operand arg2;
        Operand dest;
        std::vector<std::uint32_t> flags = {};

        std::string toString() const;
    };
}

#endif // INSTRUCTION_HPP
