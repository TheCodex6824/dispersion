/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AMD64TRANSLATIONIMPL_HPP
#define AMD64TRANSLATIONIMPL_HPP

#include <cmath>
#include <cstdint>
#include <optional>
#include <tuple>
#include <unordered_set>

#include "Translation/InstructionStream.hpp"
#include "Translation/X86/Prefix.hpp"
#include "ArchInfo/AMD64.hpp"
#include "ReaderPtr.hpp"

namespace Dispersion::Translation
{
    struct AMD64TranslationImpl
    {
        static std::uint64_t sib(std::uint8_t modrm, std::uint8_t scale, std::uint64_t index, std::uint64_t base, ReaderPtr& stream);

        enum class ArgType
        {
            Reg,
            Mem,
            Const
        };

        struct ModArgs
        {
            ArgType type1;
            std::uint64_t val1;
            ArgType type2;
            std::uint64_t val2;
        };

        static ModArgs modrm(std::uint8_t prefix, ReaderPtr& stream);

        static constexpr std::array<ArchInfo::AMD64::PhysRegId::PhysRegId, 6> SyscallParams =
        {
            ArchInfo::AMD64::PhysRegId::Di,
            ArchInfo::AMD64::PhysRegId::Si,
            ArchInfo::AMD64::PhysRegId::Dx,
            ArchInfo::AMD64::PhysRegId::R10,
            ArchInfo::AMD64::PhysRegId::R8,
            ArchInfo::AMD64::PhysRegId::R9
        };

#define OPCODE_FUNC(name) static bool name(InstructionStream& insns, std::optional<Rex::Rex> rex, const std::unordered_set<LegacyPrefix::LegacyPrefix>& prefixes, ReaderPtr& stream)

        OPCODE_FUNC(invalid);

        OPCODE_FUNC(table_0f);
        OPCODE_FUNC(syscall);

        OPCODE_FUNC(add_04);
        OPCODE_FUNC(add_05);
        OPCODE_FUNC(adc_14);
        OPCODE_FUNC(adc_15);

        OPCODE_FUNC(and_24);
        OPCODE_FUNC(and_25);

        OPCODE_FUNC(mov_88);
        OPCODE_FUNC(mov_89);
        OPCODE_FUNC(mov_8a);
        OPCODE_FUNC(mov_8b);
        OPCODE_FUNC(mov_8c);
        OPCODE_FUNC(mov_8e);
        OPCODE_FUNC(mov_a0);
        OPCODE_FUNC(mov_a1);
        OPCODE_FUNC(mov_a2);
        OPCODE_FUNC(mov_a3);
        OPCODE_FUNC(mov_b0);
        OPCODE_FUNC(mov_be);
        OPCODE_FUNC(mov_c6);
        OPCODE_FUNC(mov_c7);

        OPCODE_FUNC(lea);
#undef OPCODE_FUNC
    };
}

#endif // AMD64TRANSLATIONIMPL_HPP
