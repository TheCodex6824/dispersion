/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PREFIX_HPP
#define PREFIX_HPP

#include <cstdint>

namespace Dispersion::Translation
{
    namespace LegacyPrefix
    {
        using LegacyPrefix = std::uint8_t;

        static constexpr LegacyPrefix Lock = 0xf0;
        static constexpr LegacyPrefix RepNe = 0xf2;
        static constexpr LegacyPrefix Rep = 0xf3;
        static constexpr LegacyPrefix CsOrBranchNotTaken = 0x2e;
        static constexpr LegacyPrefix Ss = 0x36;
        static constexpr LegacyPrefix DsOrBranchTaken = 0x3e;
        static constexpr LegacyPrefix Es = 0x26;
        static constexpr LegacyPrefix Fs = 0x64;
        static constexpr LegacyPrefix Gs = 0x65;
        static constexpr LegacyPrefix OpSize = 0x66;
        static constexpr LegacyPrefix AddrSize = 0x67;
    };

    namespace Rex
    {
        using Rex = std::uint8_t;

        static constexpr Rex RexPrefix = 0x40;
        static constexpr Rex W = 0x08;
        static constexpr Rex R = 0x04;
        static constexpr Rex X = 0x02;
        static constexpr Rex B = 0x01;
    };
}

#endif // PREFIX_HPP
