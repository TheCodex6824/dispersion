/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AARCH64NATIVEINSTRUCTIONEMITTER_HPP
#define AARCH64NATIVEINSTRUCTIONEMITTER_HPP
#ifdef __aarch64__

#include "ArchInfo/AArch64.hpp"
#include "CodeGen/LiveInterval.hpp"
#include "Translation/NativeInstructionEmitter.hpp"

#include <unordered_map>

namespace Dispersion::Translation
{
    class AArch64NativeInstructionEmitter : public NativeInstructionEmitter
    {
        static std::vector<std::pair<Dispersion::Translation::RegFlags::RegFlags, Dispersion::ArchInfo::AArch64::PhysRegId::PhysRegId>> makeRegPool();

        bool enableFlags;

        void invalid(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals);
        void nop(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals);
        void load(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals);
        void add(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals);
        void sys(const Instruction& insn, std::vector<std::byte>& out, const std::vector<std::reference_wrapper<const CodeGen::LiveInterval<ArchInfo::AArch64::PhysRegId::PhysRegId>>>& intervals);

        void emitRet(std::vector<std::byte>& out);

    public:
        AArch64NativeInstructionEmitter();
        NativeBlock emitBlock(const std::pair<InstructionStream, std::uint64_t>& insns) override;
    };
}

#endif
#endif // AARCH64NATIVEINSTRUCTIONEMITTER_HPP
