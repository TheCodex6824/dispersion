/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VIRTUALREG_HPP
#define VIRTUALREG_HPP

#include <cstdint>

namespace Dispersion::Translation
{
    namespace RegFlags
    {
        using RegFlags = int;

        static constexpr int FramePointer =       0b0000000000000001;
        static constexpr int StackPointer =       0b0000000000000010;
        static constexpr int LinkReg =            0b0000000000000100;
        static constexpr int SyscallNum =         0b0000000000001000;
        static constexpr int SyscallArg1 =        0b0000000000010000;
        static constexpr int SyscallArg2 =        0b0000000000100000;
        static constexpr int SyscallArg3 =        0b0000000001000000;
        static constexpr int SyscallArg4 =        0b0000000010000000;
        static constexpr int SyscallArg5 =        0b0000000100000000;
        static constexpr int SyscallArg6 =        0b0000001000000000;
        static constexpr int SyscallArg7 =        0b0000010000000000;
        static constexpr int SyscallRet1 =        0b0000100000000000;
        static constexpr int SyscallRet2 =        0b0001000000000000;
        static constexpr int SyscallError =       0b0010000000000000;
    }

    struct VirtualReg
    {
        std::uint64_t reg;
        int flags;
    };
}

#endif // VIRTUALREG_HPP
