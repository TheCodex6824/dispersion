/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OPCODE_HPP
#define OPCODE_HPP

#include <cstdint>

namespace Dispersion::Translation
{
    enum class Opcode : std::uint32_t
    {
        Nop,
        Load,
        Store,
        Add,
        Sub,
        Mul,
        Div,
        And,
        Or,
        Xor,
        Nand,
        Nor,
        Not,
        ShiftL,
        ShiftR,
        ShiftRA,
        Jmp,
        Sys,
        LoadFlag,
        StoreFlag,
        NumOpcodes
    };

    constexpr const char* opcodeToString(Opcode value)
    {
        switch(value)
        {
        case Opcode::Nop: return "nop";
        case Opcode::Load: return "load";
        case Opcode::Store: return "store";
        case Opcode::Add: return "add";
        case Opcode::Sub: return "sub";
        case Opcode::Mul: return "mul";
        case Opcode::Div: return "div";
        case Opcode::And: return "and";
        case Opcode::Or: return "or";
        case Opcode::Xor: return "xor";
        case Opcode::Nand: return "nand";
        case Opcode::Nor: return "nor";
        case Opcode::Not: return "not";
        case Opcode::ShiftL: return "lshift";
        case Opcode::ShiftR: return "rshift";
        case Opcode::ShiftRA: return "arshift";
        case Opcode::Jmp: return "jmp";
        case Opcode::StoreFlag: return "storeflag";
        case Opcode::LoadFlag: return "loadflag";
        case Opcode::Sys: return "sys";
        default: return "INVALID";
        }
    }
}

#endif // OPCODE_HPP
