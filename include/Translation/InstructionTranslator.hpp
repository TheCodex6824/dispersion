/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INSTRUCTIONTRANSLATOR_HPP
#define INSTRUCTIONTRANSLATOR_HPP

#include "ReaderPtr.hpp"
#include "Translation/InstructionStream.hpp"

namespace Dispersion::Translation
{
    class InstructionTranslator
    {
    public:
        virtual std::pair<InstructionStream, std::uint64_t> translateBlock(ReaderPtr& in) = 0;
    };
}

#endif // INSTRUCTIONTRANSLATOR_HPP
