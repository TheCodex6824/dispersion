/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INSTRUCTIONSTREAM_HPP
#define INSTRUCTIONSTREAM_HPP

#include "Translation/Instruction.hpp"
#include "Translation/VirtualReg.hpp"

#include <span>
#include <unordered_map>
#include <vector>

namespace Dispersion::Translation
{
    class InstructionStream
    {
        std::vector<VirtualReg> regs;
        std::unordered_map<std::uint64_t, std::size_t> regLookup;
        std::vector<Instruction> insns;

    public:
        InstructionStream();

        const VirtualReg& getOrCreateReg(std::uint64_t reg, int flags);
        // TODO make safer (optional maybe)
        const VirtualReg* getReg(std::uint64_t reg) const;
        std::span<const VirtualReg> getRegs() const;

        void pushInsn(Instruction insn);
        std::span<const Instruction> getInsns() const;
    };
}

#endif // INSTRUCTION_HPP
