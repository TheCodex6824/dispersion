/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NATIVEINSTRUCTIONEMITTER_HPP
#define NATIVEINSTRUCTIONEMITTER_HPP

#include "Translation/InstructionStream.hpp"
#include "Translation/NativeBlock.hpp"

namespace Dispersion::Translation
{
    class NativeInstructionEmitter
    {
    public:
        virtual Translation::NativeBlock emitBlock(const std::pair<InstructionStream, std::uint64_t>& insns) = 0;
    };
}

#endif // NATIVEINSTRUCTIONEMITTER_HPP
