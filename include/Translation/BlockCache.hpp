/**
 * Dispersion
 * Copyright (c) 2021 TheCodex6824.
 *
 * This file is part of Dispersion.
 *
 * Dispersion is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dispersion is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Dispersion. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BLOCKCACHE_HPP
#define BLOCKCACHE_HPP

#include "ReaderPtr.hpp"
#include "Translation/InstructionTranslator.hpp"
#include "Translation/NativeBlock.hpp"
#include "Translation/NativeInstructionEmitter.hpp"

#include <unordered_map>

namespace Dispersion::Translation
{
    class BlockCache
    {
        InstructionTranslator& translator;
        NativeInstructionEmitter& emitter;
        std::unordered_map<std::uint64_t, NativeBlock> blocks;

    public:
        explicit BlockCache(InstructionTranslator& tr, NativeInstructionEmitter& e);
        ~BlockCache() noexcept;

        const NativeBlock& getOrTranslateBlock(std::uint64_t addr, ReaderPtr& ptr);
    };
}

#endif // BLOCKCACHE_HPP
